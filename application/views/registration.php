<h2>Регистрация</h2>

<?php if (isset($message)): ?>
    <div class="alert alert-warning"><?php echo $message; ?></div>
<?php endif; ?>

<?php if (isset($errors) && count($errors)):
    foreach ($errors as $error): ?>
        <div class="alert alert-danger"> <?php echo $error; ?></div>
    <?php endforeach;
endif; ?>

<div class="info"><p>Пароль должен быть не менее 6 символов длиной.</p>

    <p>*Обязательные поля</p></div>
<form class="form-horizontal" method="post">
    <div class="form-group">
        <label class="control-label col-xs-3" for="firstName">Имя:</label>

        <div class="col-xs-9">
            <input type="text" name="name" class="form-control" id="firstName" placeholder="Введите имя"
                   value="<?php echo isset($fields['name']) ? $fields['name'] : ''; ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-xs-3" for="lastName">Фамилия:</label>

        <div class="col-xs-9">
            <input type="text" name="last_name" class="form-control" id="lastName" placeholder="Введите фамилию"
                   value="<?php echo isset($fields['last_name']) ? $fields['last_name'] : ''; ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-xs-3" for="fatherName">*Логин:</label>

        <div class="col-xs-9">
            <input type="text" name="login" class="form-control" id="login" placeholder="Введите логин"
                   value="<?php echo isset($fields['login']) ? $fields['login'] : ''; ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-xs-3" for="inputEmail">*Email:</label>

        <div class="col-xs-9">
            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email"
                   value="<?php echo isset($fields['email']) ? $fields['email'] : ''; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="inputPassword">*Пароль:</label>

        <div class="col-xs-9">
            <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Введите пароль">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-3" for="confirmPassword">*Подтвердите пароль:</label>

        <div class="col-xs-9">
            <input type="password" name="password_confirm" class="form-control" id="confirmPassword"
                   placeholder="Введите пароль ещё раз">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-xs-3">*Введите символы с картинки:</label>

        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-3"><?php echo $captcha; ?></div>
                <div class="col-xs-9"><input name="captcha" type="text" class="form-control" placeholder=""></div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-3 col-xs-9">
            <input type="submit" class="btn btn-primary" value="Регистрация">
            <input type="reset" class="btn btn-default" value="Очистить форму">
        </div>
    </div>
</form>