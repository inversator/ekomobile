<div class="row">

    <div class="col-xs-12 col-sm-9">
        <div class="page-header">
            <h1><?php echo $title; ?></h1>
        </div>
        <?php if (count($cart)): ?>
            <p class="help-block">*Вы можете изменить количество товаров или удалить их из корзины</p>
            <hr>
            <table class='table table-hover table-striped' id="cartList">
                <?php foreach ($cart as $unit): ?>
                    <tr><td><span class="glyphicon glyphicon-trash delFromCart" 
                                  onclick="delFromCart(<?php echo $unit['id']; ?>);
                                                  update();
                                                  return false;">
                            </span></td><td>
                                <a href="/accum/<?php echo $unit['brand'].'/'.$unit['link']; ?>"><?php echo $unit['name']; ?></a>
                            <span class="right">
                                <span class="badge pointer" onclick="changeCountCart(<?php echo $unit['id']; ?>, -1); update(); return false;">-</span>
                                <span class="badge">x
                                    <?php echo $unit['count']; ?>
                                </span>
                                <span class="badge pointer" onclick="changeCountCart(<?php echo $unit['id']; ?>, 1); update(); return false;">+</span>
                            </span>
                        </td><td><?php echo (Discount::check($unit) ?  Discount::check($unit) : $unit['price']) * $unit['count']; ?></td></tr>
                <?php endforeach; ?>

                <tr><td></td><td>Итого</td><td><?php echo $sum; ?></td></tr>
            </table>
            <div class="row">
                <a class="col-xs-6" href="/accum/"><button style="width: 100%" class="btn btn-default" type="button">Продолжить покупки</button></a>
                <a class="col-xs-6" href="/order"><button style="width: 100%" class="btn btn-success" type="button">Оформить заказ</button></a>
            </div>
        <?php else: ?>
            <p>Вы не выбрали ни одного товара</p>
        <?php endif; ?>
    </div>

</div>
<script>
    function update()
    {
        $.get("/cart/updateMainCart",
                function (data) {
                    $('#cartList').html(data);
                }
        );
    }
</script>