<div id="login">
    <div class="col-xs-12 col-sm-6">
        <?php if ($message): ?>
            <div class="alert alert-warning"><?php echo $message; ?></div><?php endif; ?>
        <?php if (!$user): ?>
            <form role="form" class="form-signin" method="post">
                <h2 class="form-signin-heading">Войти на сайт</h2>


                <input name="login" type="text" autofocus="" required="" placeholder="Логин" class="form-control">
                <input name="password" type="password" required="" placeholder="Пароль" class="form-control">

                <button type="submit" class="btn btn-lg btn-primary btn-block logIn">Войти</button>
            </form>
        <?php else: ?>
            <h2>Ваши данные</h2>
            Вы вошли как <?php echo $user['NAME']; ?>
            <table class="table table-responsive">
                <tr>
                    <td>Ваш логин</td>
                    <td><?php echo $user['LOGIN']; ?></td>
                </tr>
                <tr>
                    <td>Ваш email</td>
                    <td><?php echo $user['EMAIL']; ?></td>
                </tr>
            </table>
        <?php endif; ?>
    </div>
    <?php if ($user): ?>
        <div class="col-xs-12 col-sm-6">
            <button onclick="window.location='/login/out/';" class="btn btn-info logOut">Выйти</button>
        </div>
        <?php else: ?>

        <div class="col-xs-12 col-sm-6">
            <hr>
            <button onclick="window.location='/login/registration/'" class="btn btn-lg btn-info btn-block">Регистрация
            </button>
        </div>
    <?php endif; ?>
</div>