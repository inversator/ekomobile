<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="description" content="<?php echo $description; ?>">
    <script src="/public/js/jquery.js"></script>
<!--    <script src="/public/js/jquery.mobile.custom/jquery.mobile.custom.js"></script>-->
    <link rel="shortcut icon" href="/favicon.ico">
    <title><?php echo $bTitle; ?></title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/public/css/main.css" rel="stylesheet">

    <script src="/public/js/offcanvas.js"></script>
    <script src="/bootstrap/js/bootstrap.js"></script>
    <script src="/public/js/main.js"></script>
</head>

<body>

<div id="load" style="display: none">
    <span><img src="/public/images/load.gif"/></span>
</div>

<?php echo $topNav; ?>
<div class="container">

    <?php echo $header; ?>

    <?php echo $midNav; ?>

    <hr>

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-10">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Категории</button>
            </p>
            <?php echo isset($pTitle) ? "<h1>" . $pTitle . "</h1>" : ''; ?>
            <?php echo $content; ?>
        </div>
        <div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">

            <?php echo $brandsNav; ?>

        </div>
    </div>

    <hr>
    <?php echo $footer; ?>
    <?php echo $modals; ?>
</div>
</body>
</html>