<div class="reviews">
    <?php foreach ($reviews->as_array() as $review): ?>
        <div class="col-xs-12 col-sm-6">
            <div class="row">
                <div class="unit">
                    <div class="col-xs-6 date">
                        <div class="">
                            <?php echo Room::date2String($review['date_create']); ?>
                        </div>
                    </div>
                    <div class="col-xs-6 title">
                        <div class="">
                            <?php echo $review['name']; ?>
                        </div>
                    </div>


                    <div class="col-sx-12">
                        <div class="text">
                            <?php echo $review['detail_text']; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>