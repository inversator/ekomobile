<div class="podbor">
    <ul class="nav nav-tabs">

        <li class="active"><a href="#car" data-toggle="tab" typecar="car">Легковые автомобили</a></li>
        <li><a href="#truck" data-toggle="tab" typecar="truck">Грузовые и автобусы</a></li>
        <li><a href="#comm" data-toggle="tab" typecar="comm">Коммерческий транспорт</a></li>

    </ul>

    <div class="tab-content">
        <div class="loadIndicator"> — Загрузка —</div>
        <div class="tab-pane active" id="car">
            <fieldset>
                <div class="autoForm row">
                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="brand" class="form-control" onchange="changeAutoSelect('car','brand')">
                            <option value="0">Марка авто</option>
                            <?php foreach ($autos['car'] as $car): ?>
                                <option value="<?php echo $car['id']; ?>"><?php echo $car['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="model" class="form-control" onchange="changeAutoSelect('car','model')">
                            <option value="">Модель авто</option>
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="modify" class="form-control" onchange="changeAutoSelect('car','modify')">
                            <option value="">Модификация</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="tab-pane" id="truck">
            <fieldset>
                <div class="autoForm row">
                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="brand" class="form-control" onchange="changeAutoSelect('truck','brand')">
                            <option value="0">Марка авто</option>
                            <?php foreach ($autos['truck'] as $car): ?>
                                <option value="<?php echo $car['id']; ?>"><?php echo $car['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="model" class="form-control" onchange="changeAutoSelect('truck','model')">
                            <option value="">Модель авто</option>
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="modify" class="form-control" onchange="changeAutoSelect('truck','modify')">
                            <option value="">Модификация</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="tab-pane" id="comm">
            <fieldset>
                <div class="autoForm row">
                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="brand" class="form-control" onchange="changeAutoSelect('comm','brand')">
                            <option value="0">Марка авто</option>
                            <?php foreach ($autos['comm'] as $car): ?>
                                <option value="<?php echo $car['id']; ?>"><?php echo $car['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="model" class="form-control" onchange="changeAutoSelect('comm','model')">
                            <option value="">Модель авто</option>
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                        <select id="modify" class="form-control" onchange="changeAutoSelect('comm','modify')">
                            <option value="">Модификация</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>

    </div>
    <div class="units row" id="units">
    </div>

</div>
<script>
    $('#brand :selected').each(function (index, value) {
        $(value).prop('selected','');
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        type = $(e.target).attr('typecar');
        changeAutoSelect(type,'brand');
    })
</script>