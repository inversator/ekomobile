<div class="facePage">
    <div class="jumbotron">
        <h1>Подобрать акб для авто</h1>

        <div class="podbor">
            <ul class="nav nav-tabs">

                <li class="active"><a href="#car" data-toggle="tab" typecar="car">Легковые автомобили</a></li>
                <li><a href="#truck" data-toggle="tab" typecar="truck">Грузовые и автобусы</a></li>
                <li><a href="#comm" data-toggle="tab" typecar="comm">Коммерческий транспорт</a></li>

            </ul>

            <div class="tab-content">
                <div class="loadIndicator"> — Загрузка —</div>
                <div class="tab-pane active" id="car">
                    <fieldset>
                        <div class="autoForm row">
                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="brand" class="form-control" onchange="changeAutoSelect('car','brand')">
                                    <option value="0">Марка авто</option>
                                    <?php foreach ($autos['car'] as $car): ?>
                                        <option value="<?php echo $car['id']; ?>"><?php echo $car['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="model" class="form-control" onchange="changeAutoSelect('car','model')">
                                    <option value="">Модель авто</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="modify" class="form-control" onchange="changeAutoSelect('car','modify')">
                                    <option value="">Модификация</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="tab-pane" id="truck">
                    <fieldset>
                        <div class="autoForm row">
                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="brand" class="form-control" onchange="changeAutoSelect('truck','brand')">
                                    <option value="0">Марка авто</option>
                                    <?php foreach ($autos['truck'] as $car): ?>
                                        <option value="<?php echo $car['id']; ?>"><?php echo $car['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="model" class="form-control" onchange="changeAutoSelect('truck','model')">
                                    <option value="">Модель авто</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="modify" class="form-control" onchange="changeAutoSelect('truck','modify')">
                                    <option value="">Модификация</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div class="tab-pane" id="comm">
                    <fieldset>
                        <div class="autoForm row">
                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="brand" class="form-control" onchange="changeAutoSelect('comm','brand')">
                                    <option value="0">Марка авто</option>
                                    <?php foreach ($autos['comm'] as $car): ?>
                                        <option value="<?php echo $car['id']; ?>"><?php echo $car['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="model" class="form-control" onchange="changeAutoSelect('comm','model')">
                                    <option value="">Модель авто</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-12 col-sm-4" id="carSelect">
                                <select id="modify" class="form-control" onchange="changeAutoSelect('comm','modify')">
                                    <option value="">Модификация</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>

            </div>
            <div class="units row" id="units">
            </div>

        </div>
    </div>
    <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#sale" data-toggle="tab">Акции и скидки</a></li>
            <li><a href="#new" data-toggle="tab">Новинки</a></li>
            <li><a href="#hits" data-toggle="tab">Хиты продаж</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="sale">
                <?php foreach ($actionUnits as $unit): ?>

                    <div class="cols-xs-12 col-sm-4">

                        <div class="image">
                            <a href='/accum/<?php echo $unit['brand'] . '/' . $unit['link']; ?>'>
                                <img
                                    src="http://www.ekoakb.ru/upload/<?php echo $unit['imgdir'] . "/" . $unit['image']; ?>"/>
                            </a>
                        </div>
                        <div class="title"><a
                                href='/accum/<?php echo $unit['brand'] . '/' . $unit['link']; ?>'><?php echo Room::subTitle($unit['title']); ?></a>
                        </div>

                        <hr>
                        <div class="priceBlock row">
                            <div class="price col-xs-8">
                                <?php
                                if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                                    echo "<s class='text-danger'>" . $unit['price'] . "</s>";
                                    echo " " . Discount::check($unit) . " руб";
                                } else {
                                    echo explode('.', $unit['price'])[0] . ' руб';
                                } ?>
                            </div>
                            <div class="buy col-xs-4"><button class="btn btn-success" onclick="addToCart(<?php echo $unit['id']; ?>)">В корзину</button></div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="tab-pane" id="new">
                <?php foreach ($newUnits as $unit): ?>

                    <div class="cols-xs-12 col-sm-4">
                        <div class="image">
                            <a href='/accum/<?php echo $unit['brand'] . '/' . $unit['link']; ?>'>
                                <img
                                    src="http://www.ekoakb.ru/upload/<?php echo $unit['imgdir'] . "/" . $unit['image']; ?>"/>
                            </a>
                        </div>
                        <div class="title"><a
                                href='/accum/<?php echo $unit['brand'] . '/' . $unit['link']; ?>'><?php echo Room::subTitle($unit['title']); ?></a>
                        </div>

                        <hr>
                        <div class="priceBlock row">
                            <div class="price col-xs-8">
                                <?php
                                if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                                    echo "<s class='text-danger'>" . $unit['price'] . "</s>";
                                    echo " " . Discount::check($unit) . " руб";
                                } else {
                                    echo explode('.', $unit['price'])[0] . ' руб';
                                } ?>
                            </div>
                            <div class="buy col-xs-4"><button class="btn btn-success" onclick="addToCart(<?php echo $unit['id']; ?>)">В корзину</button></div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="tab-pane" id="hits">
                <?php foreach ($hitUnits as $unit): ?>

                    <div class="cols-xs-12 col-sm-4">

                        <div class="image">
                            <a href='/accum/<?php echo $unit['brand'] . '/' . $unit['link']; ?>'>
                                <img
                                    src="http://www.ekoakb.ru/upload/<?php echo $unit['imgdir'] . "/" . $unit['image']; ?>"/>
                            </a>
                        </div>
                        <div class="title"><a
                                href='/accum/<?php echo $unit['brand'] . '/' . $unit['link']; ?>'><?php echo Room::subTitle($unit['title']); ?></a>
                        </div>

                        <hr>
                        <div class="priceBlock row">
                            <div class="price col-xs-8">
                                <?php
                                if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                                    echo "<s class='text-danger'>" . $unit['price'] . "</s>";
                                    echo " " . Discount::check($unit) . " руб";
                                } else {
                                    echo explode('.', $unit['price'])[0] . ' руб';
                                } ?>
                            </div>
                            <div class="buy col-xs-4"><button onclick="addToCart(<?php echo $unit['id']; ?>)" class="btn btn-success">В корзину</button></div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-xs-12">
            <h2>Ищите недорогой автомобильный аккумулятор в Москве или области?</h2>
            <h5>А можете желаете купить аккумуляторы оптом и начать собственный аккумуляторный бизнес?</h5>

            <p>В любом случае, вы попали по верному адресу – интернет-магазин EКОAKB.ru предлагает аккумуляторы для
                автомобилей любой марки и модели, как в розницу так и оптом. Наши аккумуляторы обладают солидной
                зарядной
                емкостью и обеспечивают отменную пусковую мощность и мощность холодного пуска.</p>

            <p>Обеспечить надежный запуск двигателя и «жизнедеятельность» всех электрических цепей поможет чистая
                энергия
                аккумуляторов от лучших заводов-производителей: «Курский завод «Аккумулятор», «Рязанский завод по
                переработке цветных металлов», «Источник Тока Курский» и др.</p>
        </div>
    </div>
</div>
<script>
    $('#brand :selected').each(function (index, value) {
        $(value).prop('selected','');
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        type = $(e.target).attr('typecar');
        changeAutoSelect(type,'brand');
    })

</script>