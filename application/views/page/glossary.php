<div class="reviews">
    <div class="panel-group" id="accordion">
        <?php foreach ($reviews->as_array() as $review): ?>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $review['id']; ?>">
                            <?php echo $review['name']; ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?php echo $review['id']; ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php echo $review['preview_text']; ?>
                    </div>
                </div>
            </div>


        <?php endforeach; ?>
    </div>
</div>


