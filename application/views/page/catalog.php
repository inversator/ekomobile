<div class="catalog">
    <?php
    if (isset($brands) && !empty($brands)):
        ?>

        <h3><a href="/accum">Аккумуляторы</a></h3>
        <hr>
        <div class="row">
            <?php foreach ($brands as $brand): ?>
                <div class="cols-xs-12 col-sm-4">
                    <a href="/accum/<?php echo $brand['code']; ?>"><img class="image"
                                                                        src="http://www.ekoakb.ru/upload/<?php echo $brand['image_dir'] . "/" . $brand['image']; ?>"/></a>

                    <div class="title">
                        <a href="/accum/<?php echo $brand['code']; ?>"><?php echo $brand['name']; ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    <?php else: ?>
        - категорий нет -
    <?php endif ?>
    <?php
    if (isset($cats) && !empty($cats)):
        ?>
        <h3><a href="/accessory">Аксессуары</a></h3>
        <hr>
        <div class="row">
            <?php foreach ($cats as $cat): ?>
                <div class="cols-xs-12 col-sm-4">
                    <a href="/accessory/<?php echo $cat['code']; ?>"><img class="image"
                                                                      src="http://www.ekoakb.ru/upload/<?php echo $cat['image_dir'] . "/" . $cat['image']; ?>"/></a>

                    <div class="title">
                        <a href="/accessory/<?php echo $cat['code']; ?>"><?php echo $cat['name']; ?></a>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
        <?
    else:
        ?>
        - категорий нет -
    <?php endif ?>
</div>
