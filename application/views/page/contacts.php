<div class="contacts">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4>Телефон:</h4>
            <?php echo $dealers[$city]['phone']; ?>
            <div>
                <small>Звонок по России бесплатный</small>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4>Адрес:</h4>
            <?php echo $dealers[$city]['address']; ?>
            <div>
                <small> <?php echo $dealers[$city]['schedule']; ?></small>
            </div>
        </div>
        <div class="col-xs-12">
            <h4>Пункты выдачи товара:</h4>

            <div id="map" style="width:100%;height:400px;"></div>
        </div>
    </div>
    <form id="contactForm">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Возникли вопросы? Напишите нам:</h4>
        </div>
        <div class="modal-body">
            <div class="form-group" id="name" own="name">
                <error></error>
                <input onchange="checkField()" name="name" class="form-control" type="text" placeholder="Ваше имя" required />
            </div>
            <div class="form-group" id="phone" own="phone">
                <error></error>
                <input onchange="checkField()" name="phone" class="form-control" type="text" placeholder="Ваше телефон" required />
            </div>
            <div class="form-group"  id="email" own="email">
                <error></error>
                <input onchange="checkField()" name="email" class="form-control" type="text" placeholder="Ваш email" required />
            </div>
            <div class="form-group" id="comment" own="comment">
                <error></error>
               <textarea onchange="checkField()" name="comment" class="form-control" placeholder="Ваше сообщение"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="contactMe()">Жду ответа</button>
        </div>
    </form>
</div>

<script src="//api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
<script src="//yandex.st/jquery/2.1.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map('map', {
                center: [<?php if ($city == 'www') {
                echo "55.76, 37.64";}
                else {echo $dealers[$city]['latitude'] . "," . $dealers[$city]['longitude'];}?>],
                zoom: 10
            }, {
                searchControlProvider: 'yandex#search'
            }),
            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32
            });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        objectManager.objects.options.set('preset', 'islands#greenDotIcon');
        objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
        myMap.geoObjects.add(objectManager);

        var data = {
            "type": "FeatureCollection",
            "features": [
                <?php foreach ($departments as $dep): ?>
                {
                    "type": "Feature",
                    "id": <?php echo $dep['id']; ?>,
                    "geometry": {"type": "Point", "coordinates": [<?php echo $dep['coords']; ?>]},
                    "properties": {
                        "balloonContent": '<?php
                        echo "<h4>".$dep['name']."</h4>";
                        echo $dep['address'];

                         $schedule = mysql_real_escape_string(unserialize($dep['schedule'])["TEXT"]);
                         $schedule = preg_replace("/\\\\r\\\\n/", "", $schedule);
                            echo "<br>" . $schedule;
                         ?>',
                        "clusterCaption": '<?php
                        echo $schedule;
                        ?>',
                        "hintContent": '<?php echo $dep['name']; ?>'
                    }
                },

                <?php endforeach; ?>
            ]
        }

        objectManager.add(data);


    }
</script>
