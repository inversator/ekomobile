<iframe width="100%" height="200" frameborder="0" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen=""
        src="//player.vimeo.com/video/95478998"></iframe>
<div class="jumbotron center">
    <p>Максимально выгодное предложение от компании "ЭКОАКБ".</p>

    <p>Становитесь нашим дилером прямо сейчас и Вы получите <span class="text-warning">скидку 10%</span> на нашу
        продукцию.</p>
    <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заполнить анкету дилера<br> и
        получить скидку 10%
    </button>
</div>

<h3>Выберите свой план</h3>

<ul class="nav nav-tabs">
    <li><a href="#start" data-toggle="tab">Старт <span class="help-block">При покупке 100 аккумуляторов</span>
            <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заказать</button>
        </a>

    </li>
    <li class="active"><a href="#dealer" data-toggle="tab">Дилер <span
                class="help-block">При покупке 500 аккумуляторов</span>
            <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заказать</button>
        </a></li>
    <li><a href="#repres" data-toggle="tab">Настройки <span class="help-block">При покупке 1200 аккумуляторов</span>
            <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заказать</button>
        </a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane" id="start">
        <table class="table">
            <tr>
                <td>100% гарантия от производителя</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Рекламная поддержка</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Интернет-магазин</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Высокая эффективность и рентабельность бизнеса</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Экологичность</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Маркетинг и оборудование</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Готовые бизнес-модели под Вас</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Оптимизация логистики</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Снижение накладных расходов</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Лучшая цена на новые качественные аккумуляторы ЭКОАКБ</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Лучшая цена на отработанные аккумуляторы</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Ретробонусы</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Долгосрочная забота и поддержка</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заказать</button>
                </td>
            </tr>
        </table>
    </div>
    <div class="tab-pane active" id="dealer">
        <table class="table">
            <tr>
                <td>100% гарантия от производителя</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Рекламная поддержка</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Интернет-магазин</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Высокая эффективность и рентабельность бизнеса</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Экологичность</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Маркетинг и оборудование</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Готовые бизнес-модели под Вас</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Оптимизация логистики</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Снижение накладных расходов</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Лучшая цена на новые качественные аккумуляторы ЭКОАКБ</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Лучшая цена на отработанные аккумуляторы</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Ретробонусы</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td>Долгосрочная забота и поддержка</td>
                <td><span class="glyphicon glyphicon-remove"></span></td>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заказать</button>
                </td>
            </tr>
        </table>
    </div>
    <div class="tab-pane" id="repres">
        <table class="table">
            <tr>
                <td>100% гарантия от производителя</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Рекламная поддержка</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Интернет-магазин</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Высокая эффективность и рентабельность бизнеса</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Экологичность</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Маркетинг и оборудование</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Готовые бизнес-модели под Вас</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Оптимизация логистики</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Снижение накладных расходов</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Лучшая цена на новые качественные аккумуляторы ЭКОАКБ</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Лучшая цена на отработанные аккумуляторы</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Ретробонусы</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>
            <tr>
                <td>Долгосрочная забота и поддержка</td>
                <td><span class="glyphicon glyphicon-ok green"></span></td>
            </tr>

            <tr>
                <td colspan="2" class="center">
                    <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Заказать</button>
                </td>
            </tr>

        </table>
    </div>
</div>


<div class="parner__company-features center">
    <h3>Почему становятся успешными те, кто решается<br>создать бизнес вместе с «EKOAKB»?</h3>


    <div class="col-xs-12 col-sm-6">
        <img alt="" src="http://www.ekoakb.ru/partner/1.png">

        <h3>1. Высококачественная продукция</h3>

        <p>Вы давно искали крепкого и зарекомендовавшего себя поставщика аккумуляторов? Став нашим партнером, Вы
            получите качественный товар напрямую с завода-изготовителя, со 100% гарантией возврата некачественного
            товара. Имея связь напрямую с заводом, Вы сможете приобретать именно тот товар, который необходим Вам, а
            также появится возможность для размещения уникальных заказов</p>
    </div>
    <div class="col-xs-12 col-sm-6">
        <img alt="" src="http://www.ekoakb.ru/partner/2.png">

        <h3>2. Информационная поддержка</h3>

        <p>Реклама является неотъемлемой частью продвижения товара и бизнеса. Если на слуху у покупателей будет именно
            Ваш магазин, то количество клиентов будет стремительно расти. Все большую популярность приобретают
            интернет-магазины, позволяющие делать покупки не выходя из дома. Реклама в сети интернет &ndash;
            неотъемлемая составляющая успеха.
            Став нашим партнером &ndash; Вы сразу получаете раскрученный интернет-магазин и огромное количество
            рекламной продукции. Вам не придется вкладывать в рекламу собственные деньги и время. Покупатели сами найдут
            Ваш магазин.</p>
    </div>
    <div class="col-xs-12 col-sm-6">
        <img alt="" src="http://www.ekoakb.ru/partner/3.png">

        <h3>3. Оптимизация расходов</h3>

        <p>Немаловажной частью любого бизнеса являются транспортные расходы. Вам больше не нужно ломать голову над тем,
            каким образом доставить товар до своего магазина. EKOAKB.ru обладает собственным транспортом, и мы готовы
            доставить нашу продукцию в кратчайшие сроки. В Вашем распоряжении окажется весь комплекс услуг, начиная от
            подбора заказа до доставки его в магазин без посредников.</p>
    </div>
    <div class="col-xs-12 col-sm-6">
        <img alt="" src="http://www.ekoakb.ru/partner/4.png">

        <h3>4. Уникальная система поддержки дилера ERP</h3>

        <div class="html-format">
            <p>Наша компания предлагает уникальный сервис ERP, который сделает ваше c нами взаимодействие значительно
                проще и удобней. Что включает в себя данный сервис?</p>
            <ul>
                <li>складские остатки в реальном времени</li>
                <li>контроль и аналитика оборота между дилером и поставщиком</li>
                <li>коллективный доступ к номенклатуре и заказам</li>
                <li>генерация прайс-листа и коммерческого предложения</li>
            </ul>
            <p>
                Всё это позволяет в режиме онлайн: просмотреть остатки на складе поставщика,
                сделать заявку прямо на сайте, сформировать заказ и зарезервировать его в один клик.</p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <img alt="" src="http://www.ekoakb.ru/partner/5.png">

        <h3>5. Двойная прибыль</h3>

        <p>Аккумуляторный бизнес включает два направления, которые логически дополняют друг друга &ndash; продажа новых
            аккумуляторов и сбор отработанных. Мы предлагаем Вам совместить оба направления в своем бизнесе, что
            позволит увеличить Вашу прибыль вдвое. Вы сможете получать новые аккумуляторы торговой марки EKOAKB.ru
            напрямую с завода-производителя по минимальным ценам. А также сдавать отработанные аккумуляторы напрямую нам
            на утилизацию на самых выгодных условиях.</p>
    </div>
</div>

<div class="partnerBammer center">
    <img class="image" alt="" src="http://www.ekoakb.ru/partner/accum.png">
    <button class="btn btn-success" data-target="#partnerModal" data-toggle="modal">Заполнить анкету дилера<br> и
        получить скидку 10%
    </button>
</div>

<div class="partner__company-stat">
    <h3>Что такое бренд EKOAKB сегодня?</h3>
    <ul>
        <li>
            <div><span class="company__about-number">2</span></div>
            <div><span>Года на рынке</span></div>
        </li>
        <li>
            <div><span class="company__about-number">4</span></div>
            <div><span>Страны</span></div>
        </li>
        <li>
            <div><span class="company__about-number">17</span></div>
            <div><span>Регионов</span></div>
        </li>
    </ul>
</div>

<div class="partner__steps">
    <h3>4 простых шага к успеху вместе с EKOAKB</h3>
    <ul>
        <li>
            <table>
                <tbody>
                <tr>
                    <td><span>1</span></td>
                    <td>
                        <a data-target="#partnerModal" data-toggle="modal">Заполните заявку</a> или позвоните по
                        телефону справочной
                        службы <b>+7 (800) 555-24-48</b> (бесплатно)
                    </td>
                </tr>
                </tbody>
            </table>
        </li>
        <li>
            <table>
                <tbody>
                <tr>
                    <td><span>2</span></td>
                    <td>Ознакомьтесь с коммерческим предложением, которое подготовит для Вас сотрудник «EKOAKB».</td>
                </tr>
                </tbody>
            </table>
        </li>
        <li>
            <table>
                <tbody>
                <tr>
                    <td><span>3</span></td>
                    <td>Для начала сотрудничества подпишите договор поставки и заполните бланк заказа аккумуляторов,
                        которые для Вас подготовит сотрудник «EKOAKB».
                    </td>
                </tr>
                </tbody>
            </table>
        </li>
        <li>
            <table>
                <tbody>
                <tr>
                    <td><span>4</span></td>
                    <td>Назначьте удобное для Вас время отгрузки аккумуляторов с нашего склада.</td>
                </tr>
                </tbody>
            </table>
        </li>
    </ul>

    <a data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Начать сотрудничество</a>
</div>

<div class="partnerQuestions">
    <h3>Часто задаваемые вопросы</h3>

    <div class="panel-group" id="accordion">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Сколько составляет гарантия на аккумуляторы EKOAKB.ru?
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>На все аккумуляторные батареи купленные в нашем магазине, действует гарантия производителя 2
                        года, все батареи комплектуются фирменными гарантийными талонами, для обслуживания в официальным
                        сервисном центре.</p>

                    <p>
                        Все батареи перед продажей проходят тестирование, поэтому клиент получает только проверенный
                        полностью готовый к работе аккумулятор.</p>

                    <p>
                        Наш магазин соблюдает все положения Закона РФ "О защите прав потребителей" от 07.02.1992 N
                        2300-1</p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        Какие у вас способы оплаты для организаций?
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Безналичный расчет.</p>

                    <p>
                        Мы готовы предоставить все необходимые условия для эффективной работы наших партнеров на рынке.
                        В том числе отсрочку платежа, но мы не кредитная организация и нацелены на взаимовыгодное
                        сотрудничество.</p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        Как узнать что регион свободен?
                    </a>
                </h4>
            </div>

            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Данную информацию, Вы можете:</p>

                    <p>

                    </p>
                    <ul>
                        <li>Просмотреть на <a target="_blank" href="/addresses/">карте регионов (представителей)</a>
                        </li>
                        <li>Уточнить у консультанта, позвонив по телефону <a href="tel:88005552448">8 (800) 555-2448</a>
                            (звонок бесплатный)
                        </li>
                        <li>Написать на E-mail: <a href="mailto:info@ekoakb.ru">info@ekoakb.ru</a>, в ответ на запрос,
                            Вам отправят всю интересующую информацию.
                        </li>
                    </ul>

                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        А Кто Вы такие? И какое качество ваших акб?
                    </a>
                </h4>
            </div>

            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Мы команда ЭкоАКБ. Совместный проект крупнейших заводов России по производству и переработке
                        аккумуляторов &ndash; «Курский завод «Аккумулятор» и «Рязцветмет».</p>

                    <p>
                        Курские аккумуляторы зарекомендовали себя как надежные батареи, при этом качество батарей год от
                        года улучшается.</p>

                    <p>
                        100% гарантия от производителя. Возврат по гарантийному талону.</p>

                </div>
            </div>

        </div>
    </div>

</div>
<div class="center">
    <h3>Остались вопросы? Задайте их менеджеру!</h3>
    <button data-target="#partnerModal" data-toggle="modal" class="btn btn-success">Задать вопрос</button>
</div>