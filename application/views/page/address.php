<style>
    #map {
        width: 100%; height: 100%; padding: 0; margin: 0;
    }

    #my-listbox {
        top: auto;
        left: auto;
    }
</style>

<div class="address">

    <div class="row">
        <div class="col-xs-12">

            <div id="map" style="width:100%;height:400px;"></div>
        </div>
    </div>

</div>

<script src="//api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
<script src="//yandex.st/jquery/2.1.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map('map', {
                center: [<?php if ($city == 'www') {
                echo "55.76, 37.64";}
                else {echo $dealers[$city]['latitude'] . "," . $dealers[$city]['longitude'];}?>],
                zoom: 10,
                controls: ['searchControl']
            }, {
                searchControlProvider: 'yandex#search'
            }),
            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32
            }),
        // Создадим собственный макет выпадающего списка.
            ListBoxLayout = ymaps.templateLayoutFactory.createClass(
                "<button id='my-listbox-header' class='btn btn-success dropdown-toggle' data-toggle='dropdown'>" +
                "{{data.title}} <span class='caret'></span>" +
                "</button>" +
                    // Этот элемент будет служить контейнером для элементов списка.
                    // В зависимости от того, свернут или развернут список, этот контейнер будет
                    // скрываться или показываться вместе с дочерними элементами.
                "<ul id='my-listbox'" +
                " class='dropdown-menu scrollable-menu' role='menu' aria-labelledby='dropdownMenu'" +
                " style='display: {% if state.expanded %}block{% else %}none{% endif %};'></ul>", {

                    build: function () {
                        // Вызываем метод build родительского класса перед выполнением
                        // дополнительных действий.
                        ListBoxLayout.superclass.build.call(this);

                        this.childContainerElement = $('#my-listbox').get(0);
                        // Генерируем специальное событие, оповещающее элемент управления
                        // о смене контейнера дочерних элементов.
                        this.events.fire('childcontainerchange', {
                            newChildContainerElement: this.childContainerElement,
                            oldChildContainerElement: null
                        });
                    },

                    // Переопределяем интерфейсный метод, возвращающий ссылку на
                    // контейнер дочерних элементов.
                    getChildContainerElement: function () {
                        return this.childContainerElement;
                    },

                    clear: function () {
                        // Заставим элемент управления перед очисткой макета
                        // откреплять дочерние элементы от родительского.
                        // Это защитит нас от неожиданных ошибок,
                        // связанных с уничтожением dom-элементов в ранних версиях ie.
                        this.events.fire('childcontainerchange', {
                            newChildContainerElement: null,
                            oldChildContainerElement: this.childContainerElement
                        });
                        this.childContainerElement = null;
                        // Вызываем метод clear родительского класса после выполнения
                        // дополнительных действий.
                        ListBoxLayout.superclass.clear.call(this);
                    }
                }),

        // Также создадим макет для отдельного элемента списка.
            ListBoxItemLayout = ymaps.templateLayoutFactory.createClass(
                "<li><a>{{data.content}}</a></li>"
            ),

        // Создадим 2 пункта выпадающего списка

            listBoxItems = [
                <?php foreach ($cities as $city): ?>
                new ymaps.control.ListBoxItem({
                    data: {
                        content: '<?php echo $city['city']; ?>',
                        center: [<?php echo $city['coords']; ?>],
                        zoom: 9
                    }
                }),
                <?php endforeach; ?>

                new ymaps.control.ListBoxItem({
                    data: {
                        content: 'Ясная Поляна',
                        center: [54.0762000, 37.5262861],
                        zoom: 9
                    }
                })
            ],

        // Теперь создадим список, содержащий 2 пунтка.
            listBox = new ymaps.control.ListBox({
                items: listBoxItems,
                data: {
                    title: 'Выберите город'
                },
                options: {
                    // С помощью опций можно задать как макет непосредственно для списка,
                    layout: ListBoxLayout,
                    // так и макет для дочерних элементов списка. Для задания опций дочерних
                    // элементов через родительский элемент необходимо добавлять префикс
                    // 'item' к названиям опций.
                    itemLayout: ListBoxItemLayout
                }
            });

        listBox.events.add('click', function (e) {
            // Получаем ссылку на объект, по которому кликнули.
            // События элементов списка пропагируются
            // и их можно слушать на родительском элементе.
            var item = e.get('target');
            // Клик на заголовке выпадающего списка обрабатывать не надо.
            if (item != listBox) {
                myMap.setCenter(
                    item.data.get('center'),
                    item.data.get('zoom')
                );
            }
        });

        myMap.controls.add(listBox, {float: 'left'});


        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        objectManager.objects.options.set('preset', 'islands#greenDotIcon');
        objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
        myMap.geoObjects.add(objectManager);

        var data = {
            "type": "FeatureCollection",
            "features": [
                <?php foreach ($departments as $dep): ?>
                {
                    "type": "Feature",
                    "id": <?php echo $dep['id']; ?>,
                    "geometry": {"type": "Point", "coordinates": [<?php echo $dep['coords']; ?>]},
                    "properties": {
                        "balloonContent": '<?php
                        echo "<h4>".$dep['name']."</h4>";
                        echo $dep['address'];
                        echo "<br>".$dep['phone'];
                         ?>',
                        "clusterCaption": '<?php
                         echo $dep['address'];

                        ?>',
                        "hintContent": '<?php echo $dep['name']; ?>'
                    }
                },

                <?php endforeach; ?>
            ]
        }

        objectManager.add(data);


    }
</script>
