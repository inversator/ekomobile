<div class="brand">

    <h1><?php if ($essence == 'accum') {
            echo "Купить аккумулятор " . $brand['name'];
        } else {
            echo $brand['name'];
        } ?></h1>

    <?php if ($config['showImg'] && $config['showMinPrice'] && $config['showDesc']): ?>
        <div class="row">
            <div class="cols-xs-12 col-sm-4 leftBlock center">
                <img class="image"
                     src="http://www.ekoakb.ru/upload/<?php echo $brand['imgdir'] . '/' . $brand['image']; ?>"/>
                <span class="label label-primary"> от <?php echo $minPrice; ?> руб.</span>
            </div>

            <div class="cols-xs-12 col-sm-8">
                <h2>Основные преимущества</h2>
                <?php
                if (isset($brand['options'])):
                    $advantages = explode(PHP_EOL, $brand['options']);


                    foreach ($advantages as $advantage): ?>
                        <?php if ($advantage): ?>
                            <div class="li"><span class="glyphicon glyphicon-ok green"></span> <?php echo $advantage; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>

                <?php endif; ?>

            </div>

        </div>
    <?php endif; ?>

</div>
<hr>
<div class="row unitList">
    <?php if (isset($units) && !empty($units)): ?>

        <?php foreach ($units as $unit): ?>

            <div class="cols-xs-12 col-sm-4">

                <div class="image">
                    <a href='<?php echo $brand['code'] . '/' . $unit['link']; ?>'>
                        <img
                            src="http://www.ekoakb.ru/upload/<?php echo $unit['imgdir'] . "/" . $unit['image']; ?>"/>
                    </a>
                </div>
                <div class="title"><a
                        href='<?php echo $brand['code'] . '/' . $unit['link']; ?>'><?php echo Room::subTitle($unit['title']); ?></a>
                </div>

                <div class="priceBlock row">
                    <div class="price col-xs-8">
                        <?php
                        if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                            echo "<s class='text-danger'>" . abs($unit['price']) . "</s>";
                            echo " " . Discount::check($unit) . " руб";
                        } else {
                            echo explode('.', $unit['price'])[0] . ' руб';
                        } ?>
                    </div>
                    <div class="buy col-xs-4">
                        <?php

                        if (isset($unit['property']['IN_STOCK'])): ?>
                            <button class="btn btn-success" onclick="addToCart(<?php echo $unit['id']; ?>)">В корзину
                            </button>
                        <?php else: ?>
                            <a
                                href='<?php echo $brand['code'] . '/' . $unit['link']; ?>'>   <button class="btn btn-info">Подробнее</button></a>

                            </a>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if (!empty($unit['property'])): ?>
                    <div class="properties">
                        <ul>
                            <?php if (isset($unit['property']['CAPACITY'])): ?>
                                <li><?php
                                    echo $unit['property']['CAPACITY']['name']; ?>
                                    : <?php echo $unit['property']['CAPACITY']['value']; ?>
                                </li>
                            <?php endif; ?>

                            <?php if (isset($unit['property']['CAPACITY'])): ?>
                                <li><?php echo $unit['property']['POLARITY']['name']; ?>
                                    : <?php echo $unit['property']['POLARITY']['value']; ?></li>
                            <?php endif; ?>

                            <?php if (isset($unit['property']['CAPACITY'])): ?>
                                <li><?php echo $unit['property']['SIZE']['name']; ?>
                                    : <?php echo $unit['property']['SIZE']['value']; ?></li>
                            <? endif; ?>

                            <?php if (isset($unit['property']['CAPACITY'])): ?>
                                <li><?php echo $unit['property']['VOLTAGE']['name']; ?>
                                    : <?php echo $unit['property']['VOLTAGE']['value']; ?></li>
                            <? endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <hr>
            </div>

        <?php endforeach; ?>

    <?php else: ?>
        - Нет товаров -
    <?php endif; ?>
    <div class="pad5"><?php echo $brand['description']; ?></div>
</div>