<?php
if (isset($units) && !empty($units)):
    ?>
    <h3><a href="/accessory"><?php echo $pTitle; ?></a></h3>
    <hr>
    <div class="row catUnits">
        <?php foreach ($units as $unit): ?>
            <div class="cols-xs-12 col-sm-4">
                <a href="/accessory/<?php echo $unit['code']; ?>"><img class="image"
                        src="http://www.ekoakb.ru/upload/<?php echo $unit['image_dir'] . "/" . $unit['image']; ?>"/></a>

                <div class="title">
                    <a href="/accessory/<?php echo $unit['code']; ?>"><?php echo $unit['name']; ?></a>
                </div>


            </div>
        <?php endforeach; ?>
    </div>

<?php else: ?>
    - категорий нет -
<?php endif ?>
