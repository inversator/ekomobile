<?php
if (isset($brands) && !empty($brands)):
    ?>
    <h3><a href="/accum"><?php echo $pTitle; ?></a></h3>
    <hr>
    <div class="row catUnits">
        <?php foreach ($brands as $brand): ?>
            <div class="cols-xs-12 col-sm-4">
                <a href="/accum/<?php echo $brand['code']; ?>"><img class="image"
                        src="http://www.ekoakb.ru/upload/<?php echo $brand['image_dir'] . "/" . $brand['image']; ?>"/></a>

                <div class="title">
                    <a href="/accessory/<?php echo $brand['code']; ?>"><?php echo $brand['name']; ?></a>
                </div>


            </div>
        <?php endforeach; ?>
    </div>

<?php else: ?>
    - категорий нет -
<?php endif ?>
