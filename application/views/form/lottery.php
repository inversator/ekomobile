<h2>Данные участника</h2>
<form method="post" id="lotteryForm">
    <div class="form-group">
        <label>Ваше имя *</label>
        <input name="name" class="form-control" value="<?php echo Arr::get($_GET, 'username', (isset($_SESSION['SESS_AUTH']['NAME']) ? $_SESSION['SESS_AUTH']['NAME'] : '')); ?>" />
    </div>

    <div class="form-group">
        <label>Ваш телефон </label>
        <input name="phone" class="form-control" value="" />
    </div>

    <div class="form-group">
        <label>Ваше email *</label>
        <input name="email" class="form-control" value="<?php echo Arr::get($_GET, 'useremail', (isset($_SESSION['SESS_AUTH']['EMAIL']) ? $_SESSION['SESS_AUTH']['EMAIL'] : '')); ?>" />
    </div>

    <div class="form-group">
        <label>Ваше номер *</label>
        <input name="id" class="form-control" value="<?php echo Arr::get($_GET, 'userid', (isset($_SESSION['SESS_AUTH']['USER_ID']) ? $_SESSION['SESS_AUTH']['USER_ID'] : '')); ?>" />
    </div>

    <button type="submit" class="btn btn-lg btn-success">Участвовать</button>
</form>