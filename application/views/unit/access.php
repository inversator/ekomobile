<div class="unit">
    <h1><?php echo $pTitle; ?></h1>

    <div
        class="alert alert-danger"><?php echo isset($properties['IN_STOCK']) ? $properties['IN_STOCK']['fixvalue'] : 'Нет в наличии'; ?></div>
    <div class="row">
        <div class="col-xs-12 col-sm-4 leftBlock">
            <div class="image">
                <img src="http://www.ekoakb.ru/upload/<?php echo $unit['image_dir'] . "/" . $unit['image']; ?>"/>
            </div>

            <div class="price">
                <div class="label label-success">
                    <?php

                    if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                        echo "<s class='text-danger'>" . $unit['price'] . "</s>";
                        echo " " . Discount::check($unit) . " руб";
                    } else {
                        echo explode('.', $unit['price'])[0] . ' руб';
                    } ?>
                </div>

                <div class="btn btn-default" data-toggle="modal" data-target="#custom">Под заказ</div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-8 rightBlock">
            <?php echo Room::replaceSpace($unit['preview_text']); ?>
        </div>
    </div>

    <hr>

    <?php if (isset($properties['OPTIONS'])): ?>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#text" data-toggle="tab"><h3>Описание</h3></a></li>
            <li><a href="#delivery" data-toggle="tab"><h3>Доставка и оплата</h3></a></li>
            <li><a href="#options" data-toggle="tab"><h3>Характеристики</h3></a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="text"><p><?php echo Room::replaceSpace($unit['detail_text']); ?></p></div>
            <?php if (!empty($userFields['UF_SP'])): ?>
                <div class="tab-pane" id="delivery"><?php echo Room::pre($userFields['UF_SP']); ?></div><?php endif; ?>
            <div class="tab-pane" id="options"><?php
                echo Room::pre(unserialize($properties['OPTIONS']['value'])['TEXT']);
                ?></div>
        </div>
    <?php endif; ?>

    <div class="bg-info row">
        <h3>Есть вопросы по товару или процессу покупки?</h3>

        <div class="col-xs-12 col-sm-3">
            <p>Звоните:</p>

            <p class="phone"><span>8 (800) 555-24-48</span></p>
        </div>

        <div class="col-xs-12 col-sm-3">
            <p>Пишите:</p>

            <p class="email"><a href="mailto:">info@ekoakb.ru</a></p>
        </div>

        <div class="col-xs-12 col-sm-6">
            <p>Или оставьте свой телефон, и мы оперативно свяжемся с вами:
            </p>

            <div class="input-group" own="phone">
                <error></error>
                <input type="phone" placeholder="Введите номер" id="callMe" class="form-control" required/>

                <div class="input-group-btn">
                    <button onclick="callMe()" class="btn btn-success">Перезвоните мне</button>
                </div>
            </div>
        </div>
    </div>


</div>
<?php echo $oneClick; ?>