<div class="unit">
    <h1><?php echo $pTitle; ?></h1>

    <div class="alert <?php echo isset($properties['IN_STOCK']) ? 'alert-info' : 'alert-danger'; ?>">
        <?php
        echo isset($properties['IN_STOCK']) ? $properties['IN_STOCK']['fixvalue'] : 'Нет в наличии'; ?>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 leftBlock">
            <div class="image">
                <img src="http://www.ekoakb.ru/upload/<?php
                if ($unit['d_image_dir']) {
                    echo $unit['d_image_dir'] . "/" . $unit['d_image'];
                } else {
                    echo $unit['p_image_dir'] . "/" . $unit['p_image'];
                }
                ?>"/>
            </div>

            <div class="price">
                <div class="label label-success" onclick="addToCart(<?php echo $unit['id']; ?>)">
                    <?php

                    if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                        echo "<s class='text-danger'>" . $unit['price'] . "</s>";
                        echo " " . Discount::check($unit) . " руб";
                    } else {
                        echo explode('.', $unit['price'])[0] . ' руб';
                    } ?>
                    <?php if (isset($properties['IN_STOCK'])):?>| <span>В корзину</span><?php endif; ?>
                </div>


                    <?php if (isset($properties['IN_STOCK'])):?>
                        <div class="btn btn-danger" data-toggle="modal" data-target="#oneClick">Купить в один клик</div>
                     <?php else: ?>
                        <div class="btn btn-default" data-toggle="modal" data-target="#custom">Под заказ</div>
                     <?php endif;?>

            </div>
        </div>

        <div class="col-xs-12 col-sm-8 rightBlock">
            <table class="table table-striped">
                <?php
                unset($properties['IN_STOCK']);
                foreach ($properties as $prop): ?>
                    <tr>
                        <td><?php echo $prop['name']; ?></td>
                        <td><?php echo $prop['fixvalue'] ? $prop['fixvalue'] : $prop['value']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>

    <hr>

    <div class="e-option e-page__option row">

        <div class="e-option__item col-xs-6 col-sm-2">
            <i class="ico ico-o1 e-option__ico"></i>
            <h5 class="e-option__name">Ёмкость</h5>

            <div class="e-option__note"><?php echo $properties['CAPACITY']['value']; ?> А/Ч</div>
        </div>
        <div class="e-option__item col-xs-6 col-sm-2">
            <i class="ico ico-o2 e-option__ico"></i>
            <h5 class="e-option__name">Габаритные размеры</h5>

            <div class="e-option__note"><?php echo $properties['SIZE']['value']; ?> мм</div>
        </div>


        <div class="e-option__item col-xs-6 col-sm-2">
            <i class="ico ico-o4 e-option__ico"></i>
            <h5 class="e-option__name">Обратная<br> полярность</h5>
        </div>

        <div class="e-option__item col-xs-6 col-sm-2">
            <i class="ico ico-o5 e-option__ico"></i>
            <h5 class="e-option__name">Перчатки<br> в подарок</h5>
        </div>
        <div class="e-option__item col-xs-12 col-sm-2">
            <i class="ico ico-o3 e-option__ico"></i>
            <h5 class="e-option__name">Разработан<br> для работы</h5>

            <div class="e-option__note">в Российском<br> климате</div>
        </div>


    </div>

    <ul class="nav nav-tabs">
        <?php if (!empty($unit['detail_text'])): ?>
            <li class="active"><a href="#text" data-toggle="tab"><h3>Описание</h3></a></li><?php endif; ?>
        <?php
        if (count($autos['auto']) > 0 || count($autos['truck']) > 0 || count($autos['comm']) > 0) { ?>
            <li><a href="#autos" data-toggle="tab"><h3>Подходящие автомобили</h3></a></li>
        <?php } ?>
        <?php if (!empty($userFields['UF_SP'])): ?>
            <li><a href="#delivery" data-toggle="tab"><h3>Доставка и оплата</h3></a></li><?php endif; ?>
        <?php if (!empty($userFields['UF_WARRANTY'])): ?>
            <li><a href="#guarantee" data-toggle="tab"><h3>Гарантии</h3></a></li><?php endif; ?>
    </ul>

    <div class="tab-content">
        <?php if (!empty($unit['detail_text'])): ?>
            <div class="tab-pane active" id="text"><p><?php echo Room::replaceSpace($unit['detail_text']); ?></p>
            </div><?php endif; ?>
        <div class="tab-pane" id="autos">
            <ul class="nav nav-tabs">
                <?php if (count($autos['auto']) > 0): ?>
                    <li class="active"><a href="#auto" data-toggle="tab">Леговые автомобили</a></li><?php endif; ?>
                <?php if (count($autos['truck']) > 0): ?>
                    <li><a href="#truck" data-toggle="tab">Грузовые автомобили и автобусы</a></li><?php endif; ?>
                <?php if (count($autos['comm']) > 0): ?>
                    <li><a href="#comm" data-toggle="tab">Коммерческий транспорт</a></li><?php endif; ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php foreach ($autos as $type => $auto): ?>

                    <?php if (count($auto) > 0): ?>
                        <div class="tab-pane<?php if ($type == 'auto') echo ' active'; ?>" id="<?php echo $type; ?>">
                            <div class="panel-group" id="accordion-<?php echo $type; ?>">
                                <?php
                                $currentBrand = 1;
                                foreach ($auto as $brandHash => $models):
                                    $brand = explode('_', $brandHash);
                                    ?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse"
                                                   href="#<?php echo str_replace(' ', '', $brand[1]); ?>">
                                                    <?php echo $brand[0] ?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="<?php echo str_replace(' ', '', $brand[1]); ?>"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <?php foreach ($models as $model): ?>
                                                    <p><?php
                                                        echo $model; ?></p>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>


                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php endforeach; ?>
            </div>
        </div>
        <?php if (!empty($userFields['UF_SP'])): ?>
            <div class="tab-pane" id="delivery"><?php echo Room::pre($userFields['UF_SP']); ?></div><?php endif; ?>
        <?php if (!empty($userFields['UF_WARRANTY'])): ?>
            <div class="tab-pane"
                 id="guarantee"><?php echo Room::pre($userFields['UF_WARRANTY']); ?></div><?php endif; ?>
    </div>

    <div class="bg-info row">
        <h3>Есть вопросы по товару или процессу покупки?</h3>

        <div class="col-xs-12 col-sm-3">
            <p>Звоните:</p>

            <p class="phone"><span>8 (800) 555-24-48</span></p>
        </div>

        <div class="col-xs-12 col-sm-3">
            <p>Пишите:</p>

            <p class="email"><a href="mailto:">info@ekoakb.ru</a></p>
        </div>

        <div class="col-xs-12 col-sm-6"  id="callMeBlock">
            <p>Или оставьте свой телефон, и мы оперативно свяжемся с вами:
            </p>

            <div class="form-group input-group" own="phone">
                <error></error>
                <input type="phone" placeholder="Введите номер" id="callMe" class="form-control" required />

                <div class="input-group-btn">
                    <button onclick="callMe()" class="btn btn-success">Перезвоните мне</button>
                </div>
            </div>
        </div>
    </div>


</div>
<?php echo $oneClick; ?>
<?php echo $custom; ?>
