<div class="unit">
    <h1><?php echo $pTitle; ?></h1>

    <div class="row">
        <div class="col-xs-12 col-sm-6 leftBlock">
            <div class="title"><?php echo $unit['name']; ?></div>

            <div class="alert <?php echo isset($properties['IN_STOCK']) ? 'alert-info' : 'alert-danger'; ?>">
                <?php
                echo isset($properties['IN_STOCK']) ? $properties['IN_STOCK']['fixvalue'] : 'Нет в наличии'; ?>
            </div>

            <div class="image">
                <img src="http://www.ekoakb.ru/upload/<?php
                if ($unit['d_image_dir']) {
                    echo $unit['d_image_dir'] . "/" . $unit['d_image'];
                } else {
                    echo $unit['p_image_dir'] . "/" . $unit['p_image'];
                }
                ?>"/>
            </div>

            <div class="price">
                <div class="label label-success">
                    <?php

                    if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                        echo "<s class='text-danger'>" . $unit['price'] . "</s>";
                        echo " " . Discount::check($unit) . " руб";
                    } else {
                        echo explode('.', $unit['price'])[0] . ' руб';
                    } ?>
                </div>

                <div class="btn btn-default" data-toggle="modal" data-target="#custom">Под заказ</div>
            </div>

            <table class="table table-striped">

                <tr>
                    <td><?php echo $properties['VOLTAGE']['name']; ?></td>
                    <td><?php echo $properties['VOLTAGE']['fixvalue'] ? $properties['VOLTAGE']['fixvalue'] : $properties['VOLTAGE']['value']; ?></td>
                </tr>

                <tr>
                    <td><?php echo $properties['CAPACITY']['name']; ?></td>
                    <td><?php echo $properties['CAPACITY']['fixvalue'] ? $properties['CAPACITY']['fixvalue'] : $properties['CAPACITY']['value']; ?></td>
                </tr>

                <tr>
                    <td><?php echo $properties['POLARITY']['name']; ?></td>
                    <td><?php echo $properties['POLARITY']['fixvalue'] ? $properties['POLARITY']['fixvalue'] : $properties['POLARITY']['value']; ?></td>
                </tr>

                <tr>
                    <td><?php echo $properties['SIZE']['name']; ?></td>
                    <td><?php echo $properties['SIZE']['fixvalue'] ? $properties['SIZE']['fixvalue'] : $properties['SIZE']['value']; ?></td>
                </tr>

            </table>
        </div>

        <div class="col-xs-12 col-sm-6 rightBlock">
            <div class="title">
                <a href="/accum/ekonom/<?php echo $analog['code']; ?>"><?php echo $analog['name']; ?></a>
            </div>

            <div class="alert <?php echo isset($analog['properties']['IN_STOCK']) ? 'alert-info' : 'alert-danger'; ?>">
                <?php
                echo isset($analog['properties']['IN_STOCK']) ? $analog['properties']['IN_STOCK']['fixvalue'] : 'Нет в наличии'; ?>
            </div>

            <div class="image">
                <img src="http://www.ekoakb.ru/upload/<?php
                if ($analog['d_image_dir']) {
                    echo $analog['d_image_dir'] . "/" . $analog['d_image'];
                } else {
                    echo $analog['p_image_dir'] . "/" . $analog['p_image'];
                }
                ?>"/>
            </div>

            <div class="price">
                <div class="label label-success">
                    <?php

                    if (isset($analog['discvalue']) && !empty($analog['discvalue'])) {

                        echo "<s class='text-danger'>" . $analog['price'] . "</s>";
                        echo " " . Discount::check($analog) . " руб";
                    } else {
                        echo explode('.', $analog['price'])[0] . ' руб';
                    } ?>
                </div>

                <div class="btn btn-danger" data-toggle="modal" data-target="#oneClick">Купить в один клик</div>
            </div>

            <table class="table table-striped">
                <tr>
                    <td><?php echo $analog['properties']['VOLTAGE']['name']; ?></td>
                    <td><?php echo $analog['properties']['VOLTAGE']['fixvalue'] ? $analog['properties']['VOLTAGE']['fixvalue'] : $analog['properties']['VOLTAGE']['value']; ?></td>
                </tr>

                <tr>
                    <td><?php echo $analog['properties']['CAPACITY']['name']; ?></td>
                    <td><?php echo $analog['properties']['CAPACITY']['fixvalue'] ? $analog['properties']['CAPACITY']['fixvalue'] : $analog['properties']['CAPACITY']['value']; ?></td>
                </tr>

                <tr>
                    <td><?php echo $analog['properties']['POLARITY']['name']; ?></td>
                    <td><?php echo $analog['properties']['POLARITY']['fixvalue'] ? $analog['properties']['POLARITY']['fixvalue'] : $analog['properties']['POLARITY']['value']; ?></td>
                </tr>

                <tr>
                    <td><?php echo $analog['properties']['SIZE']['name']; ?></td>
                    <td><?php echo $analog['properties']['SIZE']['fixvalue'] ? $analog['properties']['SIZE']['fixvalue'] : $analog['properties']['SIZE']['value']; ?></td>
                </tr>
            </table>
            <div class="plus"><span class="glyphicon glyphicon-plus"></span></div>
            <div class="pluses">
                <table class="table">
                    <tr>
                        <td><span class="glyphicon glyphicon-file"></span></td>
                        <td>Гарантия
                            24 месяца
                        </td>
                    </tr>
                    <tr>
                        <td><span class="glyphicon glyphicon-time"></span></td>
                        <td>30 дней на
                            возврат
                        </td>
                    </tr>
                    <tr>
                        <td><span class="glyphicon glyphicon-credit-card"></span></td>
                        <td>
                            Оплата курьеру
                            при получении
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
    <table class="table end table-condensed">
        <tr>
            <td><span class="glyphicon glyphicon-ok"></span></td>
            <td><h3>Аккумулятор EKOAKB – проверенное на практике и заслуживающее доверия решение, обеспечивающее
                    качество по разумной цене.</h3></td>
        </tr>
    </table>

    <ul class="nav nav-tabs">
        <?php if (!empty($unit['detail_text'])): ?>
            <li class="active"><a href="#text" data-toggle="tab"><h3>Описание</h3></a></li><?php endif; ?>
        <?php
        if (count($autos['auto']) > 0 || count($autos['truck']) > 0 || count($autos['comm']) > 0) { ?>
            <li><a href="#autos" data-toggle="tab"><h3>Подходящие автомобили</h3></a></li>
        <?php } ?>
        <?php if (!empty($userFields['UF_SP'])): ?>
            <li><a href="#delivery" data-toggle="tab"><h3>Доставка и оплата</h3></a></li><?php endif; ?>
        <?php if (!empty($userFields['UF_WARRANTY'])): ?>
            <li><a href="#guarantee" data-toggle="tab"><h3>Гарантии</h3></a></li><?php endif; ?>
    </ul>

    <div class="tab-content">
        <?php if (!empty($unit['detail_text'])): ?>
            <div class="tab-pane active" id="text"><p><?php echo Room::replaceSpace($unit['detail_text']); ?></p>
            </div><?php endif; ?>
        <div class="tab-pane" id="autos">
            <ul class="nav nav-tabs">
                <?php if (count($autos['auto']) > 0): ?>
                    <li class="active"><a href="#auto" data-toggle="tab">Леговые автомобили</a></li><?php endif; ?>
                <?php if (count($autos['truck']) > 0): ?>
                    <li><a href="#truck" data-toggle="tab">Грузовые автомобили и автобусы</a></li><?php endif; ?>
                <?php if (count($autos['comm']) > 0): ?>
                    <li><a href="#comm" data-toggle="tab">Коммерческий транспорт</a></li><?php endif; ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php foreach ($autos as $type => $auto): ?>

                    <?php if (count($auto) > 0): ?>
                        <div class="tab-pane<?php if ($type == 'auto') echo ' active'; ?>" id="<?php echo $type; ?>">
                            <div class="panel-group" id="accordion-<?php echo $type; ?>">
                                <?php
                                $currentBrand = 1;
                                foreach ($auto as $brandHash => $models):
                                    $brand = explode('_', $brandHash);
                                    ?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse"
                                                   href="#<?php echo str_replace(' ', '', $brand[1]); ?>">
                                                    <?php echo $brand[0] ?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="<?php echo str_replace(' ', '', $brand[1]); ?>"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <?php foreach ($models as $model): ?>
                                                    <p><?php
                                                        echo $model; ?></p>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>


                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php endforeach; ?>
            </div>
        </div>
        <?php if (!empty($userFields['UF_SP'])): ?>
            <div class="tab-pane" id="delivery"><?php echo Room::pre($userFields['UF_SP']); ?></div><?php endif; ?>
        <?php if (!empty($userFields['UF_WARRANTY'])): ?>
            <div class="tab-pane"
                 id="guarantee"><?php echo Room::pre($userFields['UF_WARRANTY']); ?></div><?php endif; ?>
    </div>

    <div class="bg-info row">
        <h3>Есть вопросы по товару или процессу покупки?</h3>

        <div class="col-xs-12 col-sm-3">
            <p>Звоните:</p>

            <p class="phone"><span>8 (800) 555-24-48</span></p>
        </div>

        <div class="col-xs-12 col-sm-3">
            <p>Пишите:</p>

            <p class="email"><a href="mailto:">info@ekoakb.ru</a></p>
        </div>

        <div class="col-xs-12 col-sm-6">
            <p>Или оставьте свой телефон, и мы оперативно свяжемся с вами:
            </p>

            <div class="input-group" own="phone">
                <error></error>
                <input type="phone" placeholder="Введите номер" id="callMe" class="form-control" required />

                <div class="input-group-btn">
                    <button onclick="callMe()" class="btn btn-success">Перезвоните мне</button>
                </div>
            </div>
        </div>
    </div>


</div>
<?php echo $oneClick; ?>
<?php echo $custom; ?>
