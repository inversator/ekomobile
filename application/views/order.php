<div class="row order">

    <div class=" col-xs-12">
        <div class="page-header">
            <h1><?php echo $title; ?></h1>
        </div>
        <?php if (count($cart)): ?>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#info">
                                Детали заказа / Контактные данные
                            </a>
                        </h4>
                    </div>
                    <div id="info" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p class="help-block">* — поля обязательные для заполнения</p>

                            <div class="row">

                                <div class="col-sm-6 col-xs-12">
                                    <table class='table table-hover table-striped' id="cartList">
                                        <?php foreach ($cart as $unit): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $unit['name']; ?>
                                                    <span class="right">

                                                        <span class="badge">x
                                                            <?php echo $unit['count']; ?>
                                                        </span>

                                                    </span>

                                                </td>
                                                <td><?php echo (Discount::check($unit) ? Discount::check($unit) : $unit['price']) * $unit['count']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                    <p class="bg-success total center">Сумма заказа: <span
                                            class="right"><?php echo $sum; ?> Рублей</span></p>

                                </div>
                                <div class="col-sm-6 col-xs-12">

                                    <div class="form-group" id="name" own="name">
                                        <label>*Ваше Имя</label>
                                        <error></error>
                                        <input onchange="checkField()" type="text" class="form-control" name="name"
                                               placeholder="Иванов Иван">
                                    </div>

                                    <div class="form-group" id="phone" own="phone">
                                        <label>*Ваш телефон</label>
                                        <error></error>
                                        <input onchange="checkField()" type="text" class="form-control" name="phone"
                                               placeholder="8 (800) 888-88-88">
                                    </div>

                                    <div class="form-group" id="email" own="email">
                                        <label>*Ваша электронная почта</label>
                                        <error></error>
                                        <input onchange="checkField()" type="text" class="form-control" name="email"
                                               placeholder="ivanovIvan@mail.com">
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Доставка</h3>

                                    <div class="form-group" id="index">
                                        <label>Индекс</label>
                                        <input onchange="checkField()" type="text" class="form-control" name="index"
                                               placeholder="123456">
                                    </div>

                                    <div class="form-group" id="city">
                                        <label>Город</label>
                                        <input onchange="checkField()" type="text" class="form-control" name="city"
                                               placeholder="Москва">
                                    </div>

                                    <div class="form-group" id="address">
                                        <label>Адрес доставки</label>
                                <textarea onchange="checkField()"
                                          placeholder="Ваш адрес доставки или оставьте пустым в случае самовывоза"
                                          rows="1" class="form-control" name="address"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">

                                    <h3>Комментарий</h3>

                                    <div class="form-group" id="comment" own="comment">
                                        <error></error>
                                        <label>Вы можете уточнить детали заказа</label>
                                <textarea onchange="checkField()" rows="4" class="form-control"
                                          name="comment"></textarea>
                                    </div>

                                    <div class="checkbox" id="call">
                                        <label>
                                            <input name="call" type="checkbox"> Перезвонить мне
                                        </label>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#delivery">
                                Способ доставки
                            </a>
                        </h4>
                    </div>
                    <div id="delivery" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><input value="self" type="radio" name="delivery" checked="checked">
                                            Самовывоз из
                                            ПВЗ <span class="glyphicon glyphicon-map-marker"></span>

                                            <p class="help-block fontMini">*Самовывоз из пункта выдачи заказов
                                                курьерской
                                                компании</p>
                                        </label>
                                        <label><input value="courier" type="radio" name="delivery"> Курьером <span
                                                class="glyphicon glyphicon-plane"></span>

                                            <p class="help-block fontMini">*Курьер доставит ваш заказ "до двери"</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 buttonOrderBlock">
                                    <button onclick="makeOrder()" type="submit"
                                            class="btn btn-order btn-success red btn-lg">
                                        Заказать
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <?php else: ?>
            <p>Вы не выбрали ни одного товара</p>
        <?php endif; ?>

        <div class="fullText"><p class="bg-info">Детали доставки и оплаты желательно заранее уточнять по телефону.
                Поставьте
                галочку в пункте "перезвонить мне" и наш менеджер свяжется с Вами для уточнения деталей</p></div>
    </div>
</div>
<script>
    function update() {
        $.get("/cart/updateMainCart",
            function (data) {
                $('#cartList').html(data);
            }
        );
    }
</script>