<!-- Это модалка покупки под заказ-->
<div class="modal fade" id="custom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Заказ товара</h4>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <th>Название</th>
                            <th>Цена</th>
                        </tr>
                        <tr>
                            <td id="accumCustomName"><?php echo $unit['name']; ?></td>
                            <td id="accumCustomCost">
                                <?php
                                if (isset($unit['discvalue']) && !empty($unit['discvalue'])) {

                                    echo " " . Discount::check($unit) . " руб";
                                } else {
                                    echo explode('.', $unit['price'])[0] . ' руб';
                                } ?></td>
                        </tr>
                    </table>
                    <p>Стоимость доставки по Москве 400 рублей,
                        <strong>если Вы сдаете старый аккумулятор доставка осуществляется БЕСПЛАТНО</strong>
                    </p>

                    <p>
                        За пределы МКАД - дополнительная плата за каждый км, 1 км - 15 рублей.</p>

                    <div class="form-group" id="customName" own="customName">
                        <label>*Ваше Имя</label>
                        <error></error>
                        <input onchange="checkField()" type="text" class="form-control" name="customName"
                               placeholder="Иванов Иван">
                    </div>

                    <div class="form-group" id="customPhone" own="customPhone">
                        <label>*Ваш телефон</label>
                        <error></error>
                        <input onchange="checkField()" type="text" class="form-control" name="customPhone"
                               placeholder="8 (800) 888-88-88">
                    </div>

                    <div class="form-group" id="customEmail" own="customEmail">
                        <label>*Ваша электронная почта</label>
                        <error></error>
                        <input onchange="checkField()" type="text" class="form-control" name="customEmail"
                               placeholder="ivanovIvan@mail.com">
                    </div>

                    <div class="form-group">
                        Город:
                        <select class="form-control" id="customCity">
                            <?php foreach ($cities as $city): ?>

                                <option value="<?php echo $city['city']; ?>"><?php echo $city['city']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <h3>Комментарий</h3>
                        <div class="form-group" id="customComment" own="customComment">
                            <error></error>
                            <label>Вы можете уточнить детали заказа</label>
                                <textarea onchange="checkField()" rows="4" class="form-control"
                                          name="customComment"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary" onclick="custom()">Заказать</button>
                </div>
            </form>
        </div>
    </div>
</div>