<!-- Это модалка партнеров-->
<div class="modal fade" id="partnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Заявка на партнёрство</h4>
                </div>
                <div class="modal-body">
                    <p>Для того что бы стать партнёром - заполните регистрационную форму:</p>

                    <div class="form-group" own="name" id="name">
                        <error></error>
                        <input onchange="checkField()" class="form-control" type="text" name="name" placeholder="Ф.И.О*" required/>
                    </div>

                    <div class="form-group" own="comment" id="comment">
                        <error></error>
                        <input onchange="checkField()" class="form-control" name="comment" type="text" id="organization"
                               placeholder="Организация"/>
                    </div>

                    <div class="form-group" own="phone" id="phone">
                        <error></error>
                        <input onchange="checkField()" class="form-control" type="text" name="phone" placeholder="Телефон*" required/>
                    </div>

                    <div own="email" class="form-group" id="email">
                        <error></error>
                        <input type="text1" placeholder="email" value="" name="email" class="form-control"
                               onchange="checkField()"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary" onclick="partnerMe()">Отправить</button>
                </div>

            </form>
        </div>
    </div>
</div>