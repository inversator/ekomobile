<!-- Это модалка группы городов-->
<div class="modal fade" id="allCities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Выберите Ваш город</h4>
            </div>
            <div class="modal-body">
                <div style="width:auto;height:auto;overflow: auto;position:relative;">
                    <div id="allCities">
                        <table class="table table-striped">
                            <tr><td><a href="/?city=www">Москва</a></td></tr>
                            <tr><td><a href="/?city=rzn">Рязань</a></td></tr>
                            <tr><td><a href="/?city=nn">Нижний Новгород</a></td></tr>
                            <tr><td><a href="/?city=ntagil">Нижний Тагил</a></td></tr>
                            <tr><td><a href="/?city=kursk">Курск</a></td></tr>
                            <tr><td><a href="/?city=orenburg">Оренбург</a></td></tr>
                            <tr><td><a href="/?city=voronezh">Воронеж</a></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>