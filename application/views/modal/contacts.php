<!-- Это модалка контактов-->
<div class="modal fade" id="callFree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
                </div>
                <div class="modal-body">
                    <p>Введите Ваш номер телефона и нажмите "заказать". Наши менеджеры свяжутся с Вами ближайшее
                        время.</p>

                    <div class="form-group">
                        <error></error>
                        <input id="callMeName" class="form-control" type="text" placeholder="Ваше имя"/>
                    </div>
                    <div class="form-group" own="phoneCall">
                        <input id="callMePhone" class="form-control" type="text" placeholder="Ваше телефон"
                               required/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button onclick="callMe2()" type="button" class="btn btn-primary">Заказать</button>
                </div>
            </form>
        </div>
    </div>
</div>