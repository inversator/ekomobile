<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div>Мы в социальных сетях:
                </div>
                <div class="socialLinks">
                    <a target="_blank" href="http://vk.com/ekoakb" class="s-icon-vk"><i></i></a>
                    <a target="_blank" href="http://www.ok.ru/group/53750046851302" class="s-icon-ok"><i></i></a>
                    <a target="_blank" href="https://www.facebook.com/ekoakb.russia" class="s-icon-fb"><i></i></a>
                    <a target="_blank" href="https://twitter.com/ekoakb_1" class="s-icon-tw"><i></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/ekoakb" class="s-icon-in"><i></i></a>
                </div>
            </div>
            <div class="col-sm-3 facePayment">
                Мы принимаем к оплате: <img src="../../../public/images/payment.png" alt=""/>
            </div>
            <div class="col-sm-3">
                <p>Контактная информация:</p>
                <p class="footerPhone">8 (800) 555-2448</p>
                <p>Звонок бесплатный</p>
                <p>Работаем с 8:00-22:00</p>
            </div>
            <div class="col-sm-3">Россия, Москва, Огородный проезд, 4с1</div>
        </div>
    </div>
</footer>