<div role="navigation" class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <!--        <div class="navbar-header">-->
        <!--            <button data-target="#topMenu" data-toggle="collapse" class="navbar-toggle" type="button">-->
        <!--                <span class="sr-only">Меню</span>-->
        <!--                <span class="icon-bar"></span>-->
        <!--                <span class="icon-bar"></span>-->
        <!--                <span class="icon-bar"></span>-->
        <!--            </button>-->
        <!--        </div>-->

        <div id="topMenu">
            <ul class="nav navbar-nav">
                <li>
                    <div class="citySel">Ваш регион:
                        <div class="btn" data-toggle="modal"
                             data-target="#allCities"><?php
                            echo $conf['name']; ?>
                            <span class="caret"></span>
                        </div>
                    </div>
                    <?php if(!isset($user)):?>
                    <a class="headLogIn" href="/login/">Вход</a>
                    <?php else: ?>
                    <div class="userTopNav">
<!--                        <i class="glyphicon-user glyphicon"></i> -->
                        <a href="/login"><?php echo $user['NAME'];?> </a> | <a href="/login/out/">Выйти</a></div>
                    <?php endif;?>
                </li>
                <li><a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

                        <div id="cart">

                            <?php

                            if (count($cart)): ?>
                                <span class="caret"></span> Сумма:
                                <span class="badge">
                                <?php

                                $sum = 0;
                                foreach ($cart as $unit) {
                                    if (isset($unit['count'])) {
                                        $sum += (Discount::check($unit) ?  Discount::check($unit) : $unit['price']) * $unit['count'];
                                    } else {
                                        $sum += (Discount::check($unit) ?  Discount::check($unit) : $unit['price']);
                                    }

                                }
                                echo $sum;
                                ?> ք
                            </span>
                                <span id="cartBlock-1">Товаров в корзине: <?php echo count($cart); ?></span>
                            <?php else: ?>
                                <span id="cartBlock-1">Корзина пуста</span>
                            <?php endif; ?>
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </div>
                    </a>
                    <div class="dropdown-menu fullCartBlock">
                        <?php if (count($cart)): ?>
                            <table class='table'>
                                <?php foreach ($cart as $unit): ?>
                                    <tr>
                                        <td><span class="glyphicon glyphicon-trash delFromCart"
                                                  onclick="delFromCart(<?php echo $unit['id']; ?>); return false;"></span>
                                        </td>
                                        <td><?php echo $unit['name']; ?><span class="badge">x
                                                <?php echo isset($unit['count']) ? $unit['count'] : '1'; ?>
                                    </span></td>
                                        <td><?php if (isset($unit['count'])) {
                                                echo (Discount::check($unit) ?  Discount::check($unit) : $unit['price']) * $unit['count'];
                                            } else {
                                                echo (Discount::check($unit) ?  Discount::check($unit) : $unit['price']);
                                            }
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td></td>
                                    <td>Итого</td>
                                    <td><?php echo $sum; ?></td>
                                </tr>
                            </table>
                            <a href="/cart">
                                <button style="width: 100%" class="btn btn-primary" type="button">Перейти в корзину
                                </button>
                            </a>
                        <?php else: ?>
                            <p>Вы не выбрали ни одного товара</p>
                        <?php endif; ?>
                    </div>
                </li>
            </ul>
        </div>

    </div>
</div>