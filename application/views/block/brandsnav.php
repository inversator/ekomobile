<div class="list-group brands">
    <h3><a href="/accum">Аккумуляторы</a></h3>
    <hr>
    <?php
    if (isset($brands) && !empty($brands)):
        foreach ($brands as $brand):
            ?>
            <a href="/accum/<?php echo $brand['code']; ?>" class="list-group-item">
                <?php echo $brand['name']; ?>
            </a>
            <?php
        endforeach;
    else:
        ?>
        - Нет категорий -
    <?php endif; ?>

    <h3><a href="/accessory">Аксессуары</a></h3>
    <hr>
    <?php
    if (isset($cats) && !empty($cats)):
        foreach ($cats as $cat):
            if ($cat['code'] == 'omyvayushchaya-zhidkost'): else:
            ?>
            <a href="/accessory/<?php echo $cat['code']; ?>" class="list-group-item">
                <?php echo $cat['name']; ?>
            </a>
            <?php endif;
        endforeach;
    else:
        ?>
        - Нет категорий -
    <?php endif; ?>
</div>