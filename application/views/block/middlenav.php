<div class="navbar navbar-fixed navbar-inverse orange" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#midMenu">
                <span class="sr-only">Категории</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="midMenu">
            <ul class="nav navbar-nav">
                <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Аккумуляторы
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="util"><a href="#ссылка">Утилизация</a></li>
                        <li><a href="/podbor">Подбор аккумулятора</a></li>
                        <li><a href="/feedbacks">Отзывы</a></li>
                        <li><a href="/catalog">Каталог</a></li>
                        <li role="separator" class="divider"></li>
                        <?php if (isset($brands) && !empty($brands)):
                            foreach ($brands as $brand):
                                ?>
                                <li><a href="/accum/<?php echo $brand['code']; ?>">
                                        <?php echo $brand['name']; ?>
                                    </a></li>
                                <?php
                            endforeach;
                        else:
                            ?>
                            - Нет категорий -
                        <?php endif; ?>
                    </ul>

                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopap="true"
                       aria-expanded="false">
                        О компании
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/about">О нас</a></li>
                        <li><a href="/feedbacks">Отзывы</a></li>
                        <li><a href="/partner">Стать партнером</a></li>
                        <li><a href="/organizations">Организациям</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                        <li><a href="/address">Адреса магазинов</a></li>
                    </ul>

                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopap="true"
                       aria-expanded="false">
                        Информация
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/info">Полезная информация</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                        <li><a href="/address">Адреса магазинов</a></li>
                        <li><a href="/glossary">Глоссарий</a></li>
                        <li><a href="/feedbacks">Отзывы</a></li>
                    </ul>

                </li>


                <li><a href="/delivery">Оплата и доставка</a></li>
                <li><a href="/guaranty">Гарантии</a></li>
                <li><a href="/contacts">Контакты</a></li>
            </ul>
        </div>
    </div>
</div>