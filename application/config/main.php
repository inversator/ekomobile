<?php
defined('SYSPATH') or die('Прямой доступ к файлу запрещен');

return array(
    'parentSite' => 'http://www.ekoakb.ru/',
    'email' => 'aleksey.markov.msk@gmail.com, a.markov@rzcm.ru, a.haryutin@rzcm.ru, y.buhanova@rzcm.ru',
    'catalog' => array(
        'www' => 'catalog',
        'rzn' => 'catalog_ryazan',
        'voronezh' => 'catalog_voronezh',
        'nn' => 'catalog_nn',
        'kursk' => 'catalog_kursk',
        'orenburg' => 'catalog_orenburg',
        'taganrog' => 'catalog_taganrog',
        'ntagil' => 'catalog_ntagil'
        ),
);
