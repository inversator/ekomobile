<?php
defined('SYSPATH') OR die('No direct script access.');

return array(
    'trusted_hosts' => array(
        'm.ekoakb.ru',
        'ekoakb.ruu',
    ),
);
