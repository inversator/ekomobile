<?php
defined('SYSPATH') or die('No direct script access.');
return array(
    'default' => array(
        'current_page' => array('source' => 'query_string', 'key' => 'list'),
        'total_items' => 0,
        'items_per_page' => 10,
        'view' => 'pagination/limited_bootstrap',
        'auto_hide' => TRUE,
        'first_page_in_url' => FALSE,
        'max_left_pages' => 5,
        'max_right_pages' => 5,
    ),
);
