<?php
defined('SYSPATH') or die('Прямой доступ запрещен.');

abstract class Controller_Base extends Controller_Template
{
    public $template = 'main';
    public $bTitle = '';

    protected $user = 0;

    protected $block_id = 2;
    protected $block_access = 9;
    protected $propList = array(
        'accum' => array(
            'cat' => array(
                0 => "CAPACITY",
                1 => "POLARITY",
                2 => "SIZE",
                3 => "VOLTAGE",
                4 => "IN_STOCK"
            ),
            'unit' => array(
                0 => "CAPACITY",
                1 => "SIZE",
                2 => "IN_STOCK",
                3 => "WEIGHT",
                4 => "MEASUREMENT",
                5 => "PREPARATION",
                6 => "RANGE",
                7 => "VOLTAGE",
                8 => "FOOD",
                9 => "VENDOR",
                10 => "ALSO",
                11 => "CRANKING",
                12 => "POLARITY"
            )),
        'acces' => array(
            'cat' => array(
                0 => "CAPACITY",
                1 => "POLARITY",
                2 => "SIZE",
                3 => "VOLTAGE",
                4 => "IN_STOCK"
            ),
            'unit' => array(
                0 => "OPTIONS"
            )
        )
    );

    public function before()
    {
        parent::before();

        // Старт сессии
        $session = Session::instance();

        // Проверяем авторизацию пользователя
        $this->user = $session->get('SESS_AUTH');

        // Выбираем город, глобально
        if ($cityChange = Arr::get($_GET, 'city', 0)) {
            if (Cookie::get('cityM', 'www') != $cityChange) {
                Cookie::set('cityM', $cityChange);
                Model::factory('Cart')->clearCart();
                $this->redirect();
            }
        }

        $city = Cookie::get('cityM', 'www');
        $dealers = Model::factory('Map')->getDealers()->as_array('code');

        $cityConf = $dealers[$city];

        $this->getId('accum');
        $this->getId('accessory');

        $brands = Model::factory('Accum')->getBrands($this->block_id)->as_array(); //Подставляем города позже
        $cats = Model::factory('Accessory')->getCats($this->block_access)->as_array();

        $footer = View::factory('block/footer');

        $this->template->content = '';
        $this->template->footer = $footer;
        $this->template->footer->conf = $cityConf;

        $this->template->keywords = 'аккумулятор, аккумулятор для авто';
        $this->template->description = 'Купить аккумулятор для автомобиля';
        $this->template->bTitle = '';

        $this->template->topNav = View::factory('block/topnav');
        $this->template->topNav->conf = $cityConf;
        // Закидываем данные пользователя в шапку
        $this->template->topNav->user = $this->user;

        // Получаем данные для корзины
        $sessionCart = $session->get('cart');

        $cartBlock = Model::factory('Cart')->getCart($this->block_id)->as_array('id');
        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                if (isset($sessionCart[$unit['id']]['count'])) {
                    $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
                }
            }
        }

        $this->template->topNav->cart = $cartBlock;

        $this->template->header = View::factory('block/header');
        $this->template->midNav = View::factory('block/middlenav');
        $this->template->midNav->brands = $brands;

        $brandsNav = View::factory('block/brandsnav');
        $brandsNav->brands = $brands;
        $brandsNav->cats = $cats;

        $this->template->brandsNav = $brandsNav;


        $modals = View::factory('modal/cities');
        $modals .= View::factory('modal/contacts');
        $this->template->modals = $modals;
    }

    public function getId($type)
    {
        if ($type == 'accum') {
            $this->block_id = Model::factory('Accum')->getCatalog(Cookie::get('cityM'), $type);
        } else {
            $this->block_access = Model::factory('Acces')->getCatalog(Cookie::get('cityM'), $type);
        }
    }

    public function after()
    {
        parent::after();
    }
}