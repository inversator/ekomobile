<?php

class Controller_Auto extends Controller_Base
{
    public $model = 'Auto';

    public function action_getAccums()
    {

        $post = $this->request->post();

        $accums = Model::factory($this->model)->getAccums($post);

        if (count($accums) > 0) {
            $accums = $accums->as_array();

            for ($i = count($accums) - 1; $i >= 0; $i--) {
                $accums[$i]['properties'] = Model::factory('Accum')->getProps($accums[$i]['id'], $this->propList['accum']['cat'])->as_array('code');
            }
        }


        $this->auto_render = 'none';
        $this->response->body(json_encode($accums));
    }

    public function action_getModels()
    {
        $post = $this->request->post();

        $models = Model::factory($this->model)->getModels($post['brand']);

        if (count($models) > 0) {
            $models = $models->as_array();
        }

        $this->auto_render = 'none';
        $this->response->body(json_encode($models));
    }

    public function action_getModify()
    {
        $post = $this->request->post();
        $model = Model::factory($this->model);

        $modifs = $model->getModify($post['model']);
        $accums = $model->getAccums($post);

        if (count($modifs) > 0) {
            $modifs = $modifs->as_array();
        }

        if (count($accums) > 0) {
            $accums = $accums->as_array();
            for ($i = count($accums) - 1; $i >= 0; $i--) {
                $accums[$i]['properties'] = Model::factory('Accum')->getProps($accums[$i]['id'], $this->propList['accum']['cat'])->as_array('code');
            }
        }

        $result = array();
        $result['modifs'] = $modifs;
        $result['accums'] = $accums;

        $this->auto_render = 'none';
        $this->response->body(json_encode($result));
    }
}