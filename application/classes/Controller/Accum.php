<?php

class Controller_Accum extends Controller_Base
{
    public $page = 'index';
    public $model = 'Accum';
    public $essence = 'accum';

    private $config = array(
        'showImg' => 1,
        'showDesc' => 1,
        'showMinPrice' => 1
    );

    public $pTitle = 'Автомобильные аккумуляторы';
    public $bTitle = 'Автомобильные аккумуляторы';
    public $description = '';
    public $keywords = '';
    protected $block_id = 2; // По умолчанию аккумуляторы москвы

    public function action_cat()
    {
        $this->getId('accum');

        $slug = $this->request->param('slug');
        if (!$slug) $this->redirect('/' . $this->essence . '/index');

        $id = $this->request->param('id');

        $model = Model::factory($this->model);
        if (!$id) {
            // Получаем товары
            $units = $model->getBrandUnits($slug, $this->block_id)->as_array('id'); //Двойка — город Москва
            $content = View::factory('category/items');

            // Забиваем свойства
            foreach ($units as $unit) {
                $units[$unit['id']]['property'] = $model->getProps($unit['id'], $this->propList['accum']['cat'])->as_array('code'); // Получаем свойства аккумуляторов для предпросмотра
            }

            $content->units = $units;

            // Получаем бренд
            $modelBrand = Model::factory('Brand');
            $brand = $modelBrand->get($slug, $this->block_id)->as_array()[0];
            $content->brand = $brand;

            // Получаем минимальную цену товара
            $minPrice = $modelBrand->getMinPrice($slug, $this->block_id);
            $content->minPrice = $minPrice;

            $content->config = $this->config;
            $content->essence = $this->essence;

            $this->template->bTitle = 'Автомобильные аккумуляторы ' . $brand['name'];

        } else {

            $unitArr = $model->getUnit($id, $this->block_id)->as_array();


            if (!count($unitArr)) {

                Room::die_dump('Совпадений нет');

            } else {
                $unit = $unitArr[0];

                // Получаем свойства
                $properties = $model->getProps($unit['id'], $this->propList['accum']['unit'])->as_array('code');

                if (!$unit['analog']) {
                    $content = View::factory('unit/ekonom');
                } else {
                    $unitAnalog = $model->getUnitCodeByCapacity($unit["analog_capacity"], $properties['POLARITY']['value'], $this->block_id);

                    if (count($unitAnalog)) {
                        $analog = $model->getUnit($unitAnalog[0]['code'], $this->block_id)->as_array();
                        if (count($analog)) $analog = $analog[0];

                    } else {
                        $analog = array(0);
                    }

                    if (isset($analog['id'])) {
                        $content = View::factory('unit/replacement');

                        // Получаем свойства аналога
                        $analogProperties = $model->getProps($analog['id'], $this->propList['accum']['unit'])->as_array('code');
                        $analog['properties'] = $analogProperties;

                        $content->analog = $analog;
                    } else {
                        $content = View::factory('unit/ekonom');
                    }
                }

                $content->properties = $properties;

                $content->oneClick = View::factory('modal/oneclick');
                $content->custom = View::factory('modal/custom');

                $cities =  Model::factory('Map')->getActiveCities();
                $content->oneClick->cities = $cities;

                // Получаем пользовательские свойства
                $userFields = $model->getUserFields($unit['brand_id'], $this->block_id)->as_array()[0];
                $content->userFields = $userFields;

                // Получаем список подходящих авто и моделей
                $autos = Model::factory('Podbor')->getList($unit['id'])->as_array('model_name');

                $autoList = array('auto' => array(), 'truck' => array(), 'comm' => array());
                foreach ($autos as $auto) {

                    switch ($auto['type']) {
                        case('MODIFY'):
                            $autoList['auto'][$auto['brand_name'] . '_' . $auto['brand_id']][] = $auto['model_name'];
                            break;

                        case('MODIFY_TRUCKS'):
                            $autoList['truck'][$auto['brand_name'] . '_' . $auto['brand_id']][] = $auto['model_name'];
                            break;

                        case('MODIFY_COMM'):
                            $autoList['comm'][$auto['brand_name'] . '_' . $auto['brand_id']][] = $auto['model_name'];
                            break;
                    }

                }

                $content->autos = $autoList;

                $content->unit = $unit;

                if (!isset($analog['id'])) {
                    $content->oneClick->unit = $unit;
                    $content->custom->unit   = $unit;
                    $content->custom->cities = $cities;
                } else {
                    $content->oneClick->unit = $analog;
                    $content->custom->unit   = $unit;
                    $content->custom->cities = $cities;
                }

                $content->pTitle = $unit['name'];
                $this->template->bTitle = $unit['name'];
            }
        }

        $this->template->content = $content;

    }


}