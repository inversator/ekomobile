<?php

class Controller_Cart extends Controller
{
    public $alias = 'Корзина';
    public $essence = 'cart';
    public $gender = 'female';
    public $model = 'Cart';

    protected $block_id = 2;
    protected $block_access = 9;

    public function getId($type)
    {
        if ($type == 'accum') {
            $this->block_id = Model::factory('Accum')->getCatalog(Cookie::get('cityM'), $type);
        } else {
            $this->block_access = Model::factory('Acces')->getCatalog(Cookie::get('cityM'), $type);
        }
    }

    public function action_index()
    {
        // Выбираем город, глобально
        if ($cityChange = Arr::get($_GET, 'city', 0)) {
            if (Cookie::get('cityM', 'www') != $cityChange) {
                Cookie::set('cityM', $cityChange);
                Model::factory('Cart')->clearCart();
                $this->redirect();
            }
        }

        $city = Cookie::get('cityM', 'www');
        $dealers = Model::factory('Map')->getDealers()->as_array('code');

        $cityConf = $dealers[$city];

        $this->getId('accum');
        $this->getId('accessory');

        $this->template = View::factory('main');

        $content = View::factory($this->essence);

        $content->title = 'Корзина';

        $sessionCart = Session::instance()->get('cart');

        $cartBlock = Model::factory('Cart')->getCart($this->block_id)->as_array('id');

        $sum = 0;

        if (count($cartBlock) > 0) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            foreach ($cartBlock as $unit) {
                $sum += (Discount::check($unit) ?  Discount::check($unit) : $unit['price']) * $unit['count'];
            }
        }

        $content->cart = $cartBlock;
        $content->sum = $sum;

        $brands = Model::factory('Accum')->getBrands($this->block_id )->as_array(); //Подставляем города позже
        $cats = Model::factory('Accessory')->getCats($this->block_access)->as_array();

        $footer = View::factory('block/footer');


        $this->template->content = '';
        $this->template->footer = $footer;
        $this->template->footer->conf = $cityConf;

        $this->template->keywords = 'аккумулятор, аккумулятор для авто';
        $this->template->description = 'Купить аккумулятор для автомобиля';
        $this->template->bTitle = '';

        $this->template->topNav = View::factory('block/topnav');
        $this->template->topNav->conf = $cityConf;

        // Получаем данные для корзины
        $sessionCart = Session::instance()->get('cart');

        $cartBlock = Model::factory('Cart')->getCart($this->block_id)->as_array('id');
        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                if(isset($sessionCart[$unit['id']]['count'])) {
                    $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
                }
            }
        }

        $this->template->topNav->cart = $cartBlock;

        $this->template->header = View::factory('block/header');
        $this->template->midNav = View::factory('block/middlenav');
        $this->template->midNav->brands = $brands;

        $brandsNav = View::factory('block/brandsnav');
        $brandsNav->brands = $brands;
        $brandsNav->cats = $cats;

        $this->template->brandsNav = $brandsNav;


        $modals = View::factory('modal/cities');
        $modals .= View::factory('modal/contacts');
        $this->template->modals = $modals;


        $this->template->content = $content;
        $this->template->bTitle = $content->title;

        $this->response->body($this->template->render());


    }

    public function action_addToCart()
    {
        $this->auto_render = FALSE;
        $id = $this->request->post('id');

        return Model::factory($this->model)->addToCart($id);
    }
    
    public function action_clearCart()
    {
        $this->auto_render = FALSE;
        return Model::factory($this->model)->clearCart();
    }

    public function action_updateCart()
    {
        $sessionCart = Session::instance()->get('cart');
        $this->getId('accum');
        $cartBlock = Model::factory('Cart')->getCart($this->block_id)->as_array('id');

        $html = array();

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            $sum = 0;
            foreach ($cartBlock as $unit) {
             
                $sum += (Discount::check($unit) ?  Discount::check($unit) : $unit['price']) * $unit['count'];
            }

            $cart = 'Сумма: <span class="badge">'.$sum.'</span>';
            $cart .= '<span class="cartBlock"> Товаров в корзине: '.count($cartBlock).'</span> ';
            $cart .= '<span class="glyphicon glyphicon-shopping-cart"></span>';

            $block = "<table class='table'>";
            foreach ($cartBlock as $unit):

                $block .= '<tr>' 
                    .'<td><span class="glyphicon glyphicon-trash delFromCart" onclick="delFromCart('.$unit['id'].'); return false;"></span></td>'
                    .'<td>'.$unit['name'].'<span class="badge">x'.$unit['count'].'</span></td>'
                    .'<td>'.(Discount::check($unit) ?  Discount::check($unit) : $unit['price']) * $unit['count'].'</td>'
                    .'</tr>';

            endforeach;
            $block .= "<tr><td></td><td>Итого</td><td>$sum</td></tr>";
            $block .= "</table>";
            $block .= '<a href="/cart"><button style="width: 100%" class="btn btn-primary" type="button">Перейти в корзину</button></a>';
        } else {
            $cart = '<span class="cartBlock">Корзина пуста</span> ';
            $cart .= '<span class="glyphicon glyphicon-shopping-cart"></span>';
            $block = '<p>Вы не выбрали ни одного товара</p>';
        }
        $html['cart'] = $cart;
        $html['block'] = $block;
        $html['city'] = $this->block_id;

        $this->response->body(json_encode($html));
    }

    public function action_updateMainCart()
    {
        $this->auto_render = FALSE;
        $this->getId('accum');
        $sessionCart = Session::instance()->get('cart');

        $cartBlock = Model::factory('Cart')->getCart($this->block_id)->as_array('id');

        $html = array();

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            $sum = 0;
            foreach ($cartBlock as $unit) {
                $sum += $unit['price'] * $unit['count'];
            }

            $block = "";
            foreach ($cartBlock as $unit):

                $block .= '<tr>'
                    .'<td><span class="glyphicon glyphicon-trash delFromCart" onclick="delFromCart('.$unit['id'].'); update(); return false;"></span></td>'
                    .'<td><a href="/'.$unit['link'].'">'.$unit['name'].'</a>'
                    .'<span class="right">
                                <span class="badge pointer" onclick="changeCountCart('.$unit['id'].', -1); update(); return false;">-</span>
                                <span class="badge">x
                                    '.$unit['count'].'
                                </span>
                                <span class="badge pointer" onclick="changeCountCart('.$unit['id'].', 1); update(); return false;">+</span>
                                </span>'
                    .'</td>'
                    .'<td>'.(Discount::check($unit) ?  Discount::check($unit) : $unit['price'])* $unit['count'].'</td>'
                    .'</tr>';

            endforeach;
            $block .= "<tr><td></td><td>Итого</td><td>$sum</td></tr>";
        } else {
            $block = '<tr><td>Вы не выбрали ни одного товара</td></tr>';
        }
        $html = $block;

        $this->response->body($html);
    }

    public function action_delFromCart()
    {
        $this->auto_render = FALSE;

        Model::factory('Cart')->delFromCart($this->request->query('id'));
    }

    public function action_changeCount()
    {
        $this->auto_render = FALSE;

        $id = $this->request->query('id');
        $sign = $this->request->query('sign');

        Model::factory('Cart')->changeCount($id, $sign);
    }
}