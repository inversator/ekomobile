<?php

class Controller_Order extends Controller_Base
{
    public $alias = 'Оформление';
    public $essence = 'order';
    public $gender = 'male';
    public $model = 'Order';

    public function action_index()
    {
        $content = View::factory($this->essence);

        $content->title = 'Оформление заказа';

        $sessionCart = session::instance()->get('cart');

        $cartBlock = Model::factory('cart')->getCart($this->block_id)->as_array('id');

        // Объединяю сессионные данные с данными модели
        $sum = 0;

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            foreach ($cartBlock as $unit) {
                $sum += (Discount::check($unit) ? Discount::check($unit) : $unit['price']) * $unit['count'];
            }
        }

        $content->cart = $cartBlock;
        $content->sum = $sum;

        $this->template->content = $content;
        $this->template->bTitle = $content->title;
    }

    public function action_make()
    {
        $this->auto_render = FALSE;

        $model = Model::factory($this->model);
        $post = $model->validation($this->request->post());

        if ($post->check()) {

            // Перетряхиваем корзину
            $sessionCart = session::instance()->get('cart');

            $cartBlock = Model::factory('cart')->getCart($this->block_id)->as_array('id');

            // Объединяю сессионные данные с данными модели
            $sum = 0;

            if (count($cartBlock)) {
                foreach ($cartBlock as $unit) {
                    $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
                }

                foreach ($cartBlock as $unit) {
                    $sum += (Discount::check($unit) ? Discount::check($unit) : $unit['price']) * $unit['count'];
                }
            }

            $block = '';

            if (Login::isAuth()) {
                $block .= "<h3>Пользователь зарегистрирован. Скидка 7%!</h3>";
            }

            $block .= "<table class='table'>";

            foreach ($cartBlock as $unit) {

                $block .= '<tr>'
                    . '<td>' . $unit['name'] . '<span> x ' . $unit['count'] . '</span></td>'
                    . '<td>' . (Discount::check($unit) ? Discount::check($unit) : $unit['price']) * $unit['count'] . '</td>'
                    . '</tr>';

            }

            $block .= "<tr><td>Итого</td><td>$sum</td></tr>";
            $block .= "</table>";

            // Отправляем сообщение
//            $config = KOHANA::$config->load('email');
//            email::connect($config);

            $view = View::factory('mail/order');
            $view->post = $this->request->post();
            $view->block = $block;

//            $to = Kohana::$config->load('main.email');
//            $subject = "Заказ #" . $orderId . " на мобильном сайте EKOAKB";
//            $from = array('manager@limage62.ru', 'EKOAKB MOBILE');

//            $sendMessage = $view;
//            email::send($to, $from, $subject, $sendMessage, $html = true);

            $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
            $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе
            if ($mail->send(Kohana::$config->load('main.email'), "ЗАКАЗ С МОБИЛЬНОГО ekoakb", $view)) $message['result'] = 1;
            else $message['result'] = 0;

        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }

        $this->response->body(json_encode($message));
    }

    public function action_check()
    {
        $this->auto_render = FALSE;

        $model = Model::factory($this->model);
        $post = $model->validation($this->request->post());

        if ($post->check()) {
            $message['result'] = 0;
            $message['error'] = 0;
        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }

        $this->response->body(json_encode($message));
    }
}