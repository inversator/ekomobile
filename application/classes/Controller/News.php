<?php

class Controller_News extends Controller_Base
{
    public $model = 'News';
    protected $_block_id = 12;

    public function action_index()
    {
        $model = Model::factory($this->model);
        
        if ($this->request->param('id')) {
            $view = View::factory('newsItem');
            $unit = $model->get($this->request->param('id'));
            $view->unit = $unit;
        } else {
            $count = $model->countWhere(array(
                'IBLOCK_ID' => $this->_block_id,
                'ACTIVE' => 'Y'));

            $pagination = Pagination::factory(array(
                    'total_items' => $count,
                    'items_per_page' => 5
                    )
                )->route_params(array(
                'directory' => strtolower($this->request->directory()),
                'controller' => strtolower($this->request->controller()),
                'action' => 'list',
            ));

            $units = $model->getAllLimWhere(
                array('IBLOCK_ID' => $this->_block_id, 'ACTIVE' => 'Y'),
                'ACTIVE_FROM DESC', $pagination->offset,
                $pagination->items_per_page
            );

            $view = View::factory('news');
            $view->units = $units->as_array();
            $view->pagination = $pagination;
        }
        
        $this->template->content = $view;
    }
}