<?php

class Controller_Sender extends Controller_Base
{

    public function action_check()
    {

        $this->auto_render = FALSE;

        $post = new Validation($this->request->post());

        $post->rule('name', 'not_empty')
            ->rule('name', 'max_length', array(':value', 100))
            ->rule('name', 'alpha', array(':value', TRUE))
            ->rule('phone', 'not_empty')
            ->rule('phone', 'phone')
            ->rule('email', 'not_empty')
            ->rule('email', 'email')
            ->rule('comment', 'max_length', array(':value', 1000));
        if ($post->check()) {
            $this->response->body(json_encode(0));
        } else {
            $this->response->body(json_encode($post->errors('comments')));
        }
    }

    public function action_oneClick()
    {
        $this->auto_render = FALSE;

        $model = Model::factory('Order');
        $post = $model->validation($this->request->post());
        if ($post->check()) {

            $unit = $this->request->post();

            $block = "<table class='table'>";


            $block .= '<tr>'
                . '<td>' . trim($unit['accum']) . '<span> x 1</span></td>'
                . '<td>' . trim($unit['cost']) . '</td>'
                . '</tr>';
            $block .= "</table>";

            $view = View::factory('mail/oneclick');
            $view->post = $this->request->post();
            $view->block = $block;

            $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
            $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе
            if ($mail->send(Kohana::$config->load('main.email'), "ЗАКАЗ С МОБИЛЬНОГО ekoakb", $view)) $message['result'] = 1;
            else $message['result'] = 0;

        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }
        $this->response->body(json_encode($message));
    }

    public function action_partner()
    {
        $this->auto_render = FALSE;

        $model = Model::factory('Order');
        $post = $model->validation($this->request->post());
        if ($post->check()) {

            $view = View::factory('mail/partner');
            $view->post = $this->request->post();

            $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
            $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе
            if ($mail->send(Kohana::$config->load('main.email'), "ПАРТНЕР С МОБИЛЬНОГО ekoakb", $view)) $message['result'] = 1;
            else $message['result'] = 0;

        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }
        $this->response->body(json_encode($message));
    }

    public function action_callme()
    {
        $this->auto_render = FALSE;

        $model = Model::factory('Phone');
        $post = $model->validation($this->request->post());
        if ($post->check()) {

            $view = View::factory('mail/callme');
            $view->post = $this->request->post();

            $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
            $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе
            if ($mail->send(Kohana::$config->load('main.email'), "ЗАКАЗ С МОБИЛЬНОГО ekoakb", $view)) $message['result'] = 1;
            else $message['result'] = 0;

        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }
        $this->response->body(json_encode($message));
    }

    public function action_contactMe()
    {
        $this->auto_render = FALSE;

        $model = Model::factory('Order');
        $post = $model->validation($this->request->post());
        if ($post->check()) {

            $view = View::factory('mail/contactme');
            $view->post = $this->request->post();

            $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
            $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе
            if ($mail->send(Kohana::$config->load('main.email'), "ВОПРОС С МОБИЛЬНОГО ekoakb", $view)) $message['result'] = 1;
            else $message['result'] = 0;

        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }
        $this->response->body(json_encode($message));
    }

    public function action_custom()
    {
        $this->auto_render = FALSE;

        $post = Model::factory('Sender')->validation($this->request->post());
        if ($post->check()) {

            $unit = $this->request->post();

            $block = "<table class='table'>";


            $block .= '<tr>'
                . '<td>' . trim($unit['accum']) . '<span> x 1</span></td>'
                . '<td>' . trim($unit['cost']) . '</td>'
                . '</tr>';
            $block .= "</table>";

            $view = View::factory('mail/custom');
            $view->post = $this->request->post();
            $view->block = $block;

            $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
            $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе

            if ($mail->send(Kohana::$config->load('main.email'), "ЗАКАЗ С МОБИЛЬНОГО ekoakb", $view)) $message['result'] = 1;
            else $message['result'] = 0;

        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }
        $this->response->body(json_encode($message));
    }

}
