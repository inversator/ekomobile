<?php

class Controller_Acces extends Controller_Base
{
    public $page = 'index';
    public $model = 'Acces';
    public $essence = 'acces';

    private $config = array(
        'showImg' => 0,
        'showDesc' => 0,
        'showMinPrice' => 0
    );

    public $pTitle = 'Автомобильные аккумуляторы';
    public $bTitle = 'Автомобильные аккумуляторы';
    public $description = '';
    public $keywords = '';

    public function action_cat()
    {
        $slug = $this->request->param('slug');
        if (!$slug) $this->redirect('/');

        $id = $this->request->param('id');

        $model = Model::factory($this->model);

        if (!$id) {

            // Получаем товары
            $units = $model->getBrandUnits($slug, $this->block_access)->as_array('id'); //Двойка — город Москва

            $content = View::factory('category/items');

            // Забиваем свойства
            foreach ($units as $unit) {

                $units[$unit['id']]['property'] = $model->getProps($unit['id'], $this->propList['acces']['cat'])->as_array('code'); // Получаем свойства аккумуляторов для предпросмотра
            }

            $content->units = $units;

            // Получаем аксессуар
            $modelBrand = Model::factory('Acces');

            $brand = $modelBrand->get($slug, $this->block_access);
            if(count($brand)) {
                $brand = $brand->as_array()[0];
            } else {
                $brand = array();
            }
            $content->brand = $brand;

            // Получаем минимальную цену аксуссуара
            $minPrice = $modelBrand->getMinPrice($slug, $this->block_access);
            $content->minPrice = $minPrice;

            $content->config = $this->config;
            $content->essence = $this->essence;

            $this->template->bTitle = $brand['name'];

        } else {
            $content = View::factory('unit/access');
            $content->oneClick = View::factory('modal/custom');

            $unit = $model->getUnit($id, $this->block_access)->as_array()[0];
            if (!count($unit)) $unit = array(0);

            // Получаем свойства
            $properties = $model->getProps($unit['id'], $this->propList['acces']['unit'])->as_array('code');
            $content->properties = $properties;

            // Получаем пользовательские свойства
            $userFields = $model->getUserFields($unit['brand_id'], $this->block_access)->as_array()[0];

            $content->userFields = $userFields;

            $content->unit = $unit;
            $content->oneClick->unit = $unit;
            $cities =  Model::factory('Map')->getActiveCities();
            $content->oneClick->cities = $cities;

            $content->pTitle = $unit['name'];
            $this->template->bTitle = $unit['name'];
        }

        $this->template->content = $content;
    }


}