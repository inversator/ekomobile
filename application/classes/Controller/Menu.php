<?php

class Controller_Menu extends Controller
{

    public function action_get()
    {

        $type = $this->request->param('id');

        $model = new Model_MenuItem();
        $items = $model->getActiveItems($type);

        $menu = View::factory('block/navigation');
        $menu->lis = '';




        // Получаем данные для корзины
        $sessionCart = Session::instance()->get('cart');

        $cartBlock = Model::factory('Cart')->getCart()->as_array('id');

        foreach ($cartBlock as $unit) {
            $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
        }

        $menu->cart = $cartBlock;


        foreach ($items as $item) {

            $menu->lis .= Menu::liMark($item['name'], $item['alias']);
        }

        $this->response->body($menu);
    }
}