<?php

// База не имеет инфоблоков со страницами. Все они прописаны буквально в файлах битрикса, поэтому просто копируем все в action-ны
class Controller_Page extends Controller_Base
{
    public $page = 'index';

    public function action_index()
    {
        $content = View::factory('page/index');
        $model = Model::factory('Accum');

        // Получаем товары по акции
        $actions = $model->getActionUnits($this->block_id);
        if (count($actions)) {
            $content->actionUnits = $actions->as_array();
        } else {
            $content->actionUnits = array();
        }
        // Получаем новинки
        $news = $model->getNewUnits($this->block_id);
        if (count($news)) {
            $content->newUnits = $news->as_array();
        } else {
            $content->newUnits = array();
        }
        // Получаем хиты
        $hits = $model->getHitUnits($this->block_id);
        if (count($news)) {
            $content->hitUnits = $hits->as_array();
        } else {
            $content->hitUnits = array();
        }

        $car = Model::factory('Auto')->getAutoList(12);
        $truck = Model::factory('Auto')->getAutoList(20);
        $comm = Model::factory('Auto')->getAutoList(21);

        $content->autos = array(
            'car' => $car->as_array('name'),
            'truck' => $truck->as_array('name'),
            'comm' => $comm->as_array('name')
        );

        $this->template->content = $content;
        $this->template->bTitle = 'Доставка аккумуляторов в Москве';
        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'Аккумуляторы для автомобиля';

        $this->template->active = $this->page;
    }

    public function action_catalog()
    {
        $this->page = 'catalog';

        $content = View::factory('page/catalog');
        $content->brands = $this->template->brandsNav->brands;
        $content->cats = $this->template->brandsNav->cats;

        $this->template->content = $content;
        $this->template->bTitle = 'Доставка аккумуляторов в Москве';
        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'Аккумуляторы для автомобиля';

        $this->template->active = $this->page;
    }

    public function action_accum()
    {
        $this->page = 'accum';
        $content = View::factory('category/accum');

        $content->brands = $this->template->brandsNav->brands;

        $content->pTitle = 'Автомобильные аккумуляторы';
        $this->template->bTitle = 'Автомобильные аккумуляторы';

        $this->template->content = $content;

        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'Аккумуляторы для автомобиля';
        $this->template->active = $this->page;
    }

    public function action_lottery()
    {
        $this->page = 'lottery';
        $content = View::factory('page/lottery');
        $post = $this->request->post();
        $send = 0; // Флаг успешного отправления

        if (!$post) {
            $content->form = View::factory('form/lottery');
        } else {
            $valid = new Validation($post);

            $valid->rule('name', 'not_empty')
                ->rule('name', 'alpha', array(':value', TRUE))
                ->rule('phone', 'phone', array(':value', array(7, 8, 10, 11)))
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('id', 'not_empty')
                ->rule('id', 'numeric');

            if ($valid->check()) {
                // Отправляем сообщение об акции
                $view = View::factory('mail/participant');
                $view->post = $post;

                $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
                $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе

                if ($mail->send(KOHANA::$config->load('main.email'), "Заявка на участие в лотерее", $view)) {
                    $send = 1;
                }

            } else {
                $content->form = View::factory('form/lottery');
                $content->errors = $valid->errors('comments');
            }
        }
        $content->send = $send;
        $this->template->content = $content;

        $this->template->bTitle = 'Лотерея от экоакб';
        $this->template->active = $this->page;
    }

    public function action_accessory()
    {
        $this->page = 'accessory';
        $content = View::factory('category/accessory');

        $content->units = $this->template->brandsNav->cats;
        $content->pTitle = 'Автомобильные аксессуары';
        $this->template->bTitle = 'Автомобильные аксессуары';

        $this->template->content = $content;

        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'Аккумуляторы для автомобиля';
        $this->template->active = $this->page;
    }

    public function action_about()
    {
        $this->page = 'about';
        $content = Room::pre(file_get_contents('http://www.ekoakb.ru/about/about_inc.php'));

        $this->template->content = $content;
        $this->template->bTitle = 'О компании';
        $this->template->pTitle = 'О компании';

        $this->template->active = $this->page;
    }

    public function action_delivery()
    {
        $this->page = 'delivery';
        $content = Room::pre(file_get_contents('http://www.ekoakb.ru/delivery/delivery_inc.php'));

        $this->template->content = $content;
        $this->template->bTitle = 'Доставка аккумуляторов в Москве';
        $this->template->pTitle = 'Оплата и доставка';

        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'автомобильные аккумуляторы доставка,доставка аккумуляторов,аккумуляторы доставка в москве,купить аккумулятор с доставкой,купить аккумулятор москва доставка,доставка акб, интернет-магазин аккумуляторов, автомобильный аккумулятор купить в москве, аккумуляторы для автомобиля с доставкой, аккумуляторы в москве';
        $this->template->active = $this->page;
    }

    public function action_organizations()
    {
        $this->page = 'organizations';
        $content = Room::pre(file_get_contents('http://www.ekoakb.ru/organizations/organizations_inc.php'));

        $this->template->content = $content;
        $this->template->bTitle = 'Организациям / EKOAKB';
        $this->template->pTitle = 'Организациям';

        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'автомобильные аккумуляторы доставка,доставка аккумуляторов,аккумуляторы доставка в москве,купить аккумулятор с доставкой,купить аккумулятор москва доставка,доставка акб, интернет-магазин аккумуляторов, автомобильный аккумулятор купить в москве, аккумуляторы для автомобиля с доставкой, аккумуляторы в москве';
        $this->template->active = $this->page;
    }

    public function action_guaranty()
    {
        $this->page = 'guaranty';
        $content = Room::pre(file_get_contents('http://www.ekoakb.ru/guaranty/guaranty_inc.php'));

        $this->template->content = $content;
        $this->template->bTitle = 'Организациям / EKOAKB';
        $this->template->pTitle = 'Организациям';

        $this->template->description = 'Автомобильные аккумуляторы с доставкой в Москве и области, самовывоз. Бесплатная доставка при сдаче старого аккумулятора';
        $this->template->keywords = 'автомобильные аккумуляторы доставка,доставка аккумуляторов,аккумуляторы доставка в москве,купить аккумулятор с доставкой,купить аккумулятор москва доставка,доставка акб, интернет-магазин аккумуляторов, автомобильный аккумулятор купить в москве, аккумуляторы для автомобиля с доставкой, аккумуляторы в москве';
        $this->template->active = $this->page;
    }

    public function action_contacts()
    {
        $this->page = 'contacts';
        $content = View::factory('page/contacts');

        // Загружаем данные
        $model = Model::factory('Map');
        $dealers = $model->getDealers();
        $departments = $model->getDepartments();

        $content->dealers = Room::treatAnArray($dealers, 'code');
        $content->departments = Room::treatAnArray($departments);

        $content->city = Cookie::get('city', 'www');

        $this->template->content = $content;
        $this->template->bTitle = 'Контакты';
        $this->template->pTitle = 'Контакты';
        $this->template->active = $this->page;
    }

    public function action_address()
    {
        $this->page = 'contacts';
        $content = View::factory('page/address');

        // Загружаем данные
        $model = Model::factory('Map');
        $dealers = $model->getDealers();
        $stores = $model->getStores();
        $cities = $model->getActiveCities();

        $content->dealers = Room::treatAnArray($dealers, 'code');
        $content->departments = Room::treatAnArray($stores);
        $content->cities = Room::treatAnArray($cities, 'city');


        $content->city = Cookie::get('city', 'www');

        $this->template->content = $content;
        $this->template->bTitle = 'Адреса магазинов';
        $this->template->pTitle = 'Адреса магазинов';
        $this->template->active = $this->page;
    }

    public function action_feedbacks()
    {
        $this->page = 'feedbacks';
        $content = View::factory('page/feedbacks');
        $reviews = Model::factory('Review')->getList();
        $content->reviews = $reviews;
        $this->template->content = $content;
        $this->template->bTitle = 'Отзывы';
        $this->template->pTitle = 'Отзывы';
        $this->template->active = $this->page;
    }

    public function action_info()
    {
        $this->page = 'info';
        $content = View::factory('page/info');
        $reviews = Model::factory('Review')->getInfoList();
        $content->reviews = $reviews;
        $this->template->content = $content;
        $this->template->bTitle = 'Полезная информация';
        $this->template->pTitle = 'Полезная информация';
        $this->template->active = $this->page;
    }

    public function action_glossary()
    {
        $this->page = 'info';
        $content = View::factory('page/glossary');
        $reviews = Model::factory('Review')->getGlossList();
        $content->reviews = $reviews;
        $this->template->content = $content;
        $this->template->bTitle = 'Полезная информация';
        $this->template->pTitle = 'Полезная информация';
        $this->template->active = $this->page;
    }

    public function action_partner()
    {
        $this->page = 'partner';
        $content = View::factory('page/partner');

        $this->template->content = $content;
        $this->template->bTitle = 'Продажа автомобильных аккумуляторов оптом по всей России';
        $this->template->pTitle = 'Автомобильные аккумуляторы оптом';
        $this->template->active = $this->page;

        $this->template->modals .= View::factory('modal/partner');

    }

    public function action_podbor()
    {
        $this->page = 'partner';
        $content = View::factory('page/podbor');

        $car = Model::factory('Auto')->getAutoList(12);
        $truck = Model::factory('Auto')->getAutoList(20);
        $comm = Model::factory('Auto')->getAutoList(21);

        $content->autos = array(
            'car' => $car->as_array('name'),
            'truck' => $truck->as_array('name'),
            'comm' => $comm->as_array('name')
        );

        $this->template->content = $content;
        $this->template->bTitle = 'Подбор аккумулятора по марке автомобиля';
        $this->template->pTitle = 'Быстрый подбор аккумулятора по марке автомобиля';
        $this->template->active = $this->page;

        $this->template->modals .= View::factory('modal/partner');
    }

}