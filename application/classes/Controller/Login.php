<?php

class Controller_Login extends Controller_Base
{
    public $alias = 'Авторизация';
    public $essence = 'login';
    public $model = 'User';

    public function action_index()
    {
        $content = View::factory('login');
        $message = '';

        $user = $this->user;
        if (!$user) {

            $model = Model::factory($this->model);

            $post = $this->request->post();

            // Если поток не пуст
            if (count($post)) {
                $arUsers = $model->get($post['login']);


                // Если есть подходящий логин
                if ($arUsers->count()) {
                    $arUser = $arUsers[0];


                    if (strlen($arUser->PASSWORD) > 32) {

                        $salt = substr($arUser->PASSWORD, 0, strlen($arUser->PASSWORD) - 32);
                        $db_password = substr($arUser->PASSWORD, -32);
                    } else {

                        $salt = "";
                        $db_password = $arUser->PASSWORD;
                    }

                    $user_password = md5($salt . $post['password']);

                    if ($user_password == $db_password) {
                        // Если пароль верен
                        $user = array(
                            "AUTHORIZED" => "Y",
                            "USER_ID" => $arUser->ID,
                            "LOGIN" => $arUser->LOGIN,
                            "EMAIL" => $arUser->EMAIL,
                            "PASSWORD_HASH" => $arUser->PASSWORD,
                            "TITLE" => $arUser->TITLE,
                            "NAME" => $arUser->NAME . ($arUser->NAME == '' || $arUser->LAST_NAME == '' ? "" : " ") . $arUser->LAST_NAME
                        );

                        $this->template->topNav->user = $_SESSION["SESS_AUTH"] = $user;


                    } else {
                        // Если неверный пароль
                        $message = 'Неверный пароль';
                    }


                } else {
                    $message = 'Неверный логин';
                }
            }
        }

        $content->user = $user;
        $content->message = $message;

        $this->template->content = $content;
        $this->template->bTitle = $this->alias;
        $this->template->pTitle = $this->alias;

    }

    public function action_out()
    {
        $this->auto_render = FALSE;

        $session = Session::instance();
        $session->delete('SESS_AUTH');
        $this->redirect('/');
    }

    public function action_registration()
    {
        $content = View::factory('registration');
        $captcha = Captcha::instance();

        $post = $this->request->post();
        if ($post) {
            if (Captcha::valid($post['captcha'])) {

                $valid = Validation::factory($post);

                $valid->rule('login', 'not_empty')
                    ->rule('login','alpha_numeric')
                    ->rule('name','alpha', array(':value',TRUE))
                    ->rule('last_name','alpha', array(':value',TRUE))
                    ->rule('name','max_length', array(':value','255'))
                    ->rule('last_name','max_length', array(':value','255'))

                    ->rule('email', 'not_empty')
                    ->rule('email','email')
                    ->rule('password', 'not_empty')
                    ->rule('password_confirm', 'not_empty')

                ->rule('confirm',  'matches', array(':validation', 'password_confirm', 'password'));

                if ($valid->check()) {
                    $model = Model::factory('User');

                    if ($res = $model->add($post)) {
                        // Присваиваем группу "Зарегистрированные"
                        $model->add_group($res[0], 5);

                        $user = array(
                            "AUTHORIZED" => "Y",
                            "USER_ID" => $res[0],
                            "LOGIN" => $post['login'],
                            "EMAIL" => $post['email'],
                            "PASSWORD_HASH" => '',
                            "NAME" => $post['name'] . ($post['name'] == '' || $post['last_name'] == '' ? "" : " ") . $post['last_name']
                        );

                        // Отправляем сообщение об акции
                        $view = View::factory('mail/action');
                        $view->user = $user;

                        $mail = new Aimail("info@ekoakb.ru"); // Создаём экземпляр класса
                        $mail->setFromName("EKOAKB MOBILE"); // Устанавливаем имя в обратном адресе
                        if ($mail->send($user['EMAIL'], "Акция от EKOAKB", $view)) $message['result'] = 1;
                        else $message['result'] = 0;

                        $_SESSION["SESS_AUTH"] = $user;

                        $this->redirect('/');
                    } else {
                        $content->message = 'Ошибка регистрации';
                    }
                } else {
                    $content->errors = $valid->errors('comments');
                    $content->fields = $post;
                }
            } else {

                $content->errors = array(
                    'captcha' => 'Слово для защиты от автоматической регистрации введено неверно'
                );
                $content->fields = $post;
            }

        } else {

        }
        $content->captcha = $captcha->render();

        $this->template->content = $content;
        $this->template->bTitle = $this->alias;
        $this->template->pTitle = $this->alias;
    }
}