<?php

class Controller_Service extends Controller_Base
{
    public $page = 'index';

    public function action_index()
    {

        $slug = $this->request->param('slug');

        if (!$slug) {
            $slug = 'service';

            $content = View::factory($slug);
        } else {
            $action = 'action_'.$slug;
            $this->$action($slug);
            return;
        }

        $this->template->content = $content;
        $this->template->bTitle = 'О компании';
        $this->template->description = 'Группа компаний Автоимпорт: Сервис грузовиков по выгодным ценам. Ремонт грузовиков.';
        $this->template->keywords = 'О компании &quot;Автоимпорт&quot;.';
        $this->template->active = $this->page;
    }

    public function action_semitrailer($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'Купить полуприцеп в ГК Автоимпорт, полуприцеп цена ';
        $this->template->description = 'Купить полуприцеп с пробегом в ГК Автоимпорт можно на очень выгодных условиях';
        $this->template->keywords = 'полуприцеп цена, купить полуприцеп, полуприцеп';
        $this->template->active = $slug;
    }

    public function action_truck($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'ГК Автоимпорт';
        $this->template->description = 'Группа компаний Автоимпорт предлагает седельные тягачи и прицепы бу; у нас вы можете купить бу тягач. Если Вы собираетесь купить тягач, обратитесь в нашу компанию.';
        $this->template->keywords = 'тягачи, седельные тягачи, купить тягач, продажа тягачей';
        $this->template->active = $slug;
    }

    public function action_mileage($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'Группа компаний Автоимпорт: ';
        $this->template->description = '';
        $this->template->keywords = '';
        $this->template->active = $slug;
    }

    public function action_special($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'Спецтехника в Рязани от ГК Автоимпорт: всегда в наличии';
        $this->template->description = 'Группа компаний Автоимпорт: продажа спецтехники, продажа тягачей; у нас можно купить бу прицепы, бу техника, вольво бу, бу тягачи.';
        $this->template->keywords = 'спецтехника рязань, спецтехника';
        $this->template->active = $slug;
    }

    public function action_truck_parts($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'Автозапчасти для грузовиков. Магазин запчастей для грузовых автомобилей. Запчасти для грузовиков';
        $this->template->description = 'Магазин автозапчастей для грузовых машин в Рязани. Запчасти для грузовиков, заходите!';
        $this->template->keywords = 'Запчасти для грузовиков автозапчасти для грузовиков запчасти для грузовых машин запчасти для грузовых автомобилей';
        $this->template->active = $slug;
    }

    public function action_cleaning($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'Сервис по ремонту грузовых автомобилей. Грузовой сервис. Мойка для грузовиков.';
        $this->template->description = 'Сервис по ремонту грузовых автомобилей и грузовая мойка в Рязани на выгодных условиях. Грузовой сервис. Мойка грузовиков.';
        $this->template->keywords = 'Сервис для грузовиков грузовой сервис сервис по ремонту грузовых автомобилей грузовая мойка мойка для грузовиков';
        $this->template->active = $slug;
    }

    public function action_tahografy($slug)
    {
        $content = View::factory($slug);

        $this->template->content = $content;
        $this->template->bTitle = 'Установка, калибровка и активация тахографов. Установка системы глонави в Рязани. Изготовление карт водителей.';
        $this->template->description = 'Установка, калибровка и активация тахографов в Рязани по выгодным ценам! Заходите!';
        $this->template->keywords = 'Установка и активация тахографов активация тахографов изготовление карт водителей установка системы глонави в Рязани глонави Рязань установка тахографов калибровка тахографов.';
        $this->template->active = $slug;
    }
}