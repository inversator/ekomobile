<?php

# Общий класс для полезных функций

class Room
{
    public static function randString($pass_len=10, $pass_chars=false)
    {
        static $allchars = "abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ0123456789";
        $string = "";
        if (is_array($pass_chars)) {
            while (strlen($string) < $pass_len) {
                if (function_exists('shuffle'))
                    shuffle($pass_chars);
                foreach ($pass_chars as $chars) {
                    $n = strlen($chars) - 1;
                    $string .= $chars[mt_rand(0, $n)];
                }
            }
            if (strlen($string) > count($pass_chars))
                $string = substr($string, 0, $pass_len);
        } else {
            if ($pass_chars !== false) {
                $chars = $pass_chars;
                $n = strlen($pass_chars) - 1;
            } else {
                $chars = $allchars;
                $n = 61; //strlen($allchars)-1;
            }
            for ($i = 0; $i < $pass_len; $i++)
                $string .= $chars[mt_rand(0, $n)];
        }
        return $string;
    }

    public static function pre($text)
    {
        $patterns = array(
            '~font-size:(.*?)~iu',
            '/<\/?br\W*?\/?>/',
            '~style="(.*?)"~iu'
        );

        $result = preg_replace($patterns, '', $text);

        return $result;

    }

    public static function syspath()
    {
        defined('SYSYPATH') || die('Прямой доступ запрещен');
    }

    // Перевод размера данных
    public static function dataSize($Bytes)
    {
        $Type = array("", "Кило", "Мега", "Гига", "Тера");
        $counter = 0;
        while ($Bytes >= 1024) {
            $Bytes /= 1024;
            $counter++;
        }
        return ("" . substr($Bytes, 0, 3) . " " . $Type[$counter] . "Байтов");
    }

    public static function die_dump($value)
    {
        die("<code>" . var_dump($value) . "</code>");
    }

    public static function subDesc($text)
    {
        if (strlen($text) > 100) return substr($text, 0, 100) . "...";
        else return $text;
    }

    public static function subTitle($text)
    {
        if (strlen($text) > 90) return substr($text, 0, 90) . "...";
        else return $text;
    }

    /*
     * В случае отстутствия картинки выводится заглушка подходящих размеров
     */

    public static function showImg($img, $width = 300, $height = 300)
    {
        if (!$img) {
            $img = $width . 'x' . $height . '.png';
        }
        return $img;
    }

    public static function noImg($text)
    {
        $way = '<!img';
        $replace = str_replace('<img', $way, $text);
        return $replace;
    }

    public static function replaceSpace($text)
    {
        return str_replace("\n", "</p><p>", $text);
    }

    public static function checkRemote($url)
    {
        $Headers = get_headers($url);

        $length = explode(': ', $Headers[4]);

        if ((int)$length[1] > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function titleLink($url)
    {
        if (!$url) return;
        $url = 'http://' . parse_url($url, PHP_URL_HOST);
        //проверяем, если кириллический домен, то конвертируем его
        if (preg_match('/[а-яА-Я]/i', $url)) {
            require_once('modules/idna_convert.class.php');
            $convert = new idna_convert();
            $url = $convert->encode($url);
        }
        $meta = "";
        //получаем удаленную страницу
        @$page = file_get_contents($url);
        self::die_dump($page);
        if ($page) {
            //находим и выдираем титул
            if (preg_match("~<title>(.*?)</title>~iu", $page, $out)) {

                $meta['title'] = $out[1];
                //конвертируем в utf-8
                mb_convert_encoding($meta['title'], 'utf8');
            } else {
                $meta['title'] = '';
            }
            if (preg_match('~<meta name="description" content="(.*?)" />~iu', $page, $out)) {
                $meta['desc'] = $out[1];
                //конвертируем в utf-8
                mb_convert_encoding($meta['desc'], 'utf8');
            } else {
                $meta['desc'] = '';
            }
            if (preg_match('~<meta name="keywords" content="(.*?)" />~iu', $page, $out)) {
                $meta['key'] = $out[1];
                //конвертируем в utf-8
                mb_convert_encoding($meta['key'], 'utf8');
            } else {
                {
                    $meta['key'] = '';
                }
            }
        }
        return $meta;
    }

    public static function date2String($date)
    {
        $rusMonth = array(
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря'
        );

        $blocks = explode('-', explode(' ', $date)[0]);
        $mydata = abs($blocks[2]) . " " . $rusMonth[abs($blocks[1])] . " " . $blocks[0];
        return $mydata;
    }

    public static function treatAnArray($array, $key = 0)
    {
        if (count($array)) {
            if ($key)
                $result = $array->as_array($key);
            else
                $result = $array->as_array();

        } else {
            $result = array(0);
        }

        return $result;
    }

    public static function getCity()
    {

    }
}