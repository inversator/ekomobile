<?php

class asincPag
{
    public $itemsPerPage = 12;
    public $totalItems = 12;
    public $offset = 0;

    public function create($totalItems, $currentPage = 1, $itemsPerPage = 0)
    {
        $this->totalItems = $totalItems;

        if ($itemsPerPage) {
            $this->itemsPerPage = $itemsPerPage;
        }

        if ($this->itemsPerPage < $this->totalItems) {
            
            $pageNum = 1;
            $arr = array();

            while ($pageNum <= ceil($this->totalItems / $this->itemsPerPage)) {

                if ($pageNum != $currentPage) {
                    $arr[$pageNum] = 0;
                } else {
                    $arr[$pageNum] = 1;
                }
                
                $pageNum++;
            }
            
            $view = View::factory('asinc_pag');
            $view->nums = $arr;
            if(!$currentPage) $currentPage = 1;
            $this->offset = ($currentPage - 1) * $this->itemsPerPage;
            
            return $view->render();
        }
        
        return;
    }
}