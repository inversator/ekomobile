<?php

class Login
{
    public static function isAuth()
    {
        if (isset($_SESSION['SESS_AUTH']))
            return $_SESSION['SESS_AUTH']['AUTHORIZED'];
        else return FALSE;
    }
}
