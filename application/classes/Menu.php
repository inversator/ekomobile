<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Menu
{
    public $directory = '/';

    public static function liMark($alias, $label)
    {
        $directory = strtolower(Request::current()->directory());

        $query = Request::current()->query();

        if ($directory) {
            $directory = "/".$directory;
            $slug = strtolower($query['controller']);
        } else {
            $slug = strtolower($query['controller']);
        }

        $active = ($slug == $alias) ? " class='active'" : "";

        return "<li".$active."><a href='".$directory."/".$alias."'>".$label."</a></li>";
    }

    public static function aList($list)
    {

        $html = '';

        $slug = Request::initial()->param('slug');
        $slug2 = Request::initial()->param('slug2');
        
        foreach ($list as $item) {
            if ($item['url'] == $slug) {
                $active = ' active';
            } else {
                $active = '';
            }

            $html .= "<a href=".'/'.$item['url']." class='list-group-item".$active."'>".$item['pTitle'];
            if (count($item['dot_records'])) {
                $html .= '<span onclick="return false;" class="glyphicon glyphicon-chevron-down expand" data-toggle="collapse" data-target="#list-'.$item['id'].'"></span>';
            }
            $html .= '</a>';

            if (count($item['dot_records'])) {

                $dotList = '';
                $in = '';
                
                foreach ($item['dot_records'] as $dot_record) {
                    if ($dot_record['url'] == $slug2 && $item['url'] == $slug) {
                        $active = ' active';
                        $in = ' in';
                    } else {
                        $active = '';
                    }
                    $dotList .= "<a href = ".'/'.$item['url'].'/'.$dot_record['url']." class = 'list-group-item child".$active."'>".$dot_record['pTitle']."</a>";
                }

                $html .= '<div id="list-'.$item['id'].'" class="collapse'.$in.'">'.$dotList.'</div>';
            }
        }
        echo $html;
    }
}