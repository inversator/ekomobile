<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Order extends Model_Base
{
    protected $_table_name = 'orders';
    protected $_essence = 'order';
    protected $_table_columns = array(
        'id' => NULL,
        'name' => NULL,
        'phone' => NULL,
        'email' => NULL,
        'index' => NULL,
        'city' => NULL,
        'address' => NULL,
        'comment' => NULL,
        'call' => NULL,
        'delivery' => NULL,
        'datetime' => NULL
    );

    protected function rules()
    {

        return array(
            'name' => array(
                array('not_empty'),
                array('max_length', array(':value', 255))
            ),
            'phone' => array(
                array('not_empty'),
                array('phone', array(':value', TRUE))
            ),
            'email' => array(
                array('not_empty'),
                array('email')
            ),
            'index' => array(
                array('numeric')
            ),
            'city' => array(
                array('max_length', array(':value', 255))
            ),

        );
    }

    public function getOrderProducts($id_order)
    {
        return DB::Select('u.pTitle', 'u.url', 'o.count', 'p.value')
                ->from(array('units', 'u'))
                ->join(array('property_values', 'p'))
                ->on('p.id_unit', '=', 'u.id')
                ->and_where('p.id_prop', '=', '1')
                ->join(array('ordered_products', 'o'))
                ->on('u.id', '=', 'o.id_prod')
                ->and_where('o.id_order', '=', ':id_order')
                ->param(':id_order', $id_order)
                ->execute()
        ;
    }

    public function getByEmail($email)
    {
        return DB::Select(
                'o.id', 'o.datetime', 'o.delivery', 'o.address', 'o.city',
                'o.index', 'o.phone')
                ->from(array('orders', 'o'))
                ->where('email', '=', $email)
                ->order_by('datetime','DESC')
                ->execute()
        ;
    }
}