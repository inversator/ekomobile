<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 25.07.2016
 * Time: 12:50
 */
class Model_Map extends Model_Base
{
    public function getDealers()
    {
        return DB::select('bie.name', 'bie.code', 'bie.id',
            array('address.value', 'address'),
            array('phone.value', 'phone'),
            array('schedule.value', 'schedule'),
            array('latitude.value', 'latitude'),
            array('longitude.value', 'longitude'))
            ->from(array('b_iblock_element', 'bie'))
            ->join(array('b_iblock_element_property', 'address'))
            ->on('bie.id', '=', 'address.iblock_element_id')
            ->on('address.iblock_property_id', '=', DB::expr(85))
            ->join(array('b_iblock_element_property', 'phone'))
            ->on('bie.id', '=', 'address.iblock_element_id')
            ->on('phone.iblock_property_id', '=', DB::expr(84))
            ->join(array('b_iblock_element_property', 'schedule'))
            ->on('bie.id', '=', 'schedule.iblock_element_id')
            ->on('schedule.iblock_property_id', '=', DB::expr(86))
            ->join(array('b_iblock_element_property', 'latitude'), 'LEFT')
            ->on('bie.id', '=', 'latitude.iblock_element_id')
            ->on('latitude.iblock_property_id', '=', DB::expr(687))
            ->join(array('b_iblock_element_property', 'longitude'), 'LEFT')
            ->on('bie.id', '=', 'longitude.iblock_element_id')
            ->on('longitude.iblock_property_id', '=', DB::expr(688))
            ->where('bie.iblock_id', '=', '18')
            ->group_by('bie.id')
            ->execute();
    }

    public function getDepartments()
    {
        return DB::select('bie.name', 'bie.id',
            array('address.value', 'address'),
            array('schedule.value', 'schedule'),
            array('coords.value', 'coords'))
            ->from(array('b_iblock_element', 'bie'))
            ->join(array('b_iblock_element_property', 'address'), 'LEFT')
            ->on('bie.id', '=', 'address.iblock_element_id')
            ->on('address.iblock_property_id', '=', DB::expr(64))
            ->join(array('b_iblock_element_property', 'schedule'), 'LEFT')
            ->on('bie.id', '=', 'schedule.iblock_element_id')
            ->on('schedule.iblock_property_id', '=', DB::expr(65))
            ->join(array('b_iblock_element_property', 'coords'), 'LEFT')
            ->on('bie.id', '=', 'coords.iblock_element_id')
            ->on('coords.iblock_property_id', '=', DB::expr(63))
            ->where('bie.iblock_id', '=', '14')
            ->group_by('bie.id')
            ->execute();
    }

    public function getStores()
    {
        return DB::select('bie.name', 'bie.id',
            array('address.value', 'address'),
            array('coords.value', 'coords'),
            array('phone.value', 'phone'),
            array('bis.name','city')
        )
            ->from(array('b_iblock_element', 'bie'))
            ->join(array('b_iblock_element_property', 'phone'), 'LEFT')
            ->on('bie.id', '=', 'phone.iblock_element_id')
            ->on('phone.iblock_property_id', '=', DB::expr(39))
            ->join(array('b_iblock_element_property', 'address'), 'LEFT')
            ->on('bie.id', '=', 'address.iblock_element_id')
            ->on('address.iblock_property_id', '=', DB::expr(38))
            ->join(array('b_iblock_element_property', 'coords'), 'LEFT')
            ->on('bie.id', '=', 'coords.iblock_element_id')
            ->on('coords.iblock_property_id', '=', DB::expr(37))

            ->join(array('b_iblock_section','bis'))
            ->on('bie.iblock_section_id','=','bis.id')

            ->where('bie.iblock_id', '=', '8')
            ->group_by('bie.id')
            ->execute();
    }

    public function getActiveCities()
    {
        return DB::select('bie.name', 'bie.id',
            array('address.value', 'address'),
            array('coords.value', 'coords'),
            array('phone.value', 'phone'),
            array('bis.name','city')
        )
            ->from(array('b_iblock_element', 'bie'))
            ->join(array('b_iblock_element_property', 'phone'), 'LEFT')
            ->on('bie.id', '=', 'phone.iblock_element_id')
            ->on('phone.iblock_property_id', '=', DB::expr(39))
            ->join(array('b_iblock_element_property', 'address'), 'LEFT')
            ->on('bie.id', '=', 'address.iblock_element_id')
            ->on('address.iblock_property_id', '=', DB::expr(38))
            ->join(array('b_iblock_element_property', 'coords'), 'LEFT')
            ->on('bie.id', '=', 'coords.iblock_element_id')
            ->on('coords.iblock_property_id', '=', DB::expr(37))

            ->join(array('b_iblock_section','bis'))
            ->on('bie.iblock_section_id','=','bis.id')

            ->where('bie.iblock_id', '=', '8')
            ->group_by('city')
            ->order_by('city')
            ->execute();
    }
}