<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Sender extends Model_Base
{
    protected function rules()
    {
        return array(
            'customName' => array(
                array('not_empty'),
                array('max_length', array(':value', 255))
            ),
            'customPhone' => array(
                array('not_empty'),
                array('phone', array(':value', TRUE))
            ),
            'customEmail' => array(
                array('not_empty'),
                array('email')
            ),
            'customIndex' => array(
                array('numeric')
            ),
            'customCity' => array(
                array('max_length', array(':value', 255))
            ),

        );
    }
}