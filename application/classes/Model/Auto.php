<?php

class Model_Auto extends Model_Base
{
    protected $_table_name = 'b_iblock_section';
    protected $_table_columns = array(
        'ID',
        'ACTIVE',
        'NAME',
        'PICTURES',
        'DETAIL_TEXT',
    );

    public function getAutoList($type)
    {
        return DB::select(
            's.id',
            's.name',
            's.code'
        )
            ->from(array($this->_table_name, 's'))
            ->join(array('b_file', 'f'))
            ->on('s.PICTURE', '=', 'f.ID')
            ->where('s.ACTIVE', '=', 'Y')
            ->and_where('s.IBLOCK_ID', '=', $type)
            ->and_where('s.IBLOCK_SECTION_ID', 'IS', NULL)
            ->order_by('s.name')
            ->execute();
    }

    public function getModels($brand_id)
    {
        return DB::select(
            's.name',
            's.id'
        )
            ->from(array('b_iblock_section','s'))
            ->where('s.IBLOCK_SECTION_ID','=',$brand_id)
            ->and_where('s.ACTIVE','=','Y')
            ->order_by('s.name')
            ->execute();
    }

    public function getModify($model_id)
    {
        $query =  "SELECT  BE.ID as id,BE.NAME as name
			FROM
			b_iblock B
			INNER JOIN b_lang L ON B.LID=L.LID
			INNER JOIN b_iblock_element BE ON BE.IBLOCK_ID = B.ID
			 INNER JOIN (
					SELECT DISTINCT BSE.IBLOCK_ELEMENT_ID
					FROM b_iblock_section_element BSE

					INNER JOIN b_iblock_section BS ON BSE.IBLOCK_SECTION_ID = BS.ID

					WHERE ((BS.ID IN (:model_id)))
					) BES ON BES.IBLOCK_ELEMENT_ID = BE.ID

			WHERE 1=1
			AND (

				(((BE.IBLOCK_ID IN ('12', '20', '21'))))
				AND ((((BE.ACTIVE='Y'))))
			)
			AND (((BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL)))

		 ORDER BY BE.NAME asc  LIMIT 0, 10";

        return DB::query(1, $query)->param(':model_id', $model_id)->execute();
        //Room::die_dump($query);
    }

    public function getAccums(array $arr)
    {
        if (count($arr) > 0) {

            $query = DB::select(
                'a.name',
                'a.id',
                'a.code',

                array('fp.file_name', 'image'),
                array('fp.subdir', 'image_dir'),

                array('p.VALUE_NUM','price'),
                array('d.name','discount'),
                array('d.VALUE_TYPE', 'disctype'),
                array('d.VALUE','discvalue'),

                array('s.code','brand')
            )->from(array('b_iblock_element','a'))
                ->join(array('b_iblock_section','s'))
                ->on('s.id','=','a.iblock_section_id')

                ->join(array('b_file', 'fp'), 'LEFT')
                ->on('fp.id', '=', 'a.PREVIEW_PICTURE')

                ->join(array('b_iblock_2_index','p'))
                ->on('p.ELEMENT_ID','=','a.ID')
                ->join(array('b_catalog_discount2product','dr'), 'LEFT')
                ->on('dr.PRODUCT_ID','=','a.ID')
                ->join(array('b_catalog_discount','d'), 'LEFT')
                ->on('d.ID','=','dr.DISCOUNT_ID')
            ;

            if (!empty($arr['modify'])) {
                $query
                    ->join(array('b_iblock_element_property','r'))
                    ->on('a.id','=','r.IBLOCK_ELEMENT_ID')
                    ->on('r.value','=',DB::expr($arr['modify']))
                ;
            } else {
                $query->join(array('b_iblock_element_property','r'))
                    ->on('a.id','=','r.IBLOCK_ELEMENT_ID')
                ->join(array('b_iblock_element','mf'))
                ->on('r.value','=','mf.id')
                ->on('mf.iblock_section_id','=',DB::expr($arr['model']))
                ;
            }

            $query
                ->and_where('a.ACTIVE', '=', 'Y')
                ->and_where('p.VALUE_NUM','>','0')
                ->group_by('a.id')
            ;

            return $query->execute();

        } else {
            return array(0);
        }
    }

    public function getAuto($id)
    {
        return DB::select('a.name', 'a.id', 'a.detail_text')
            ->from(array($this->_table_name, 'a'))
            ->where('a.ID', '=', $id)
            ->group_by('a.id')
            ->execute();
    }

    public function getBrands($id_block)
    {
        return DB::select('s.name', array(DB::expr('COUNT(a.id)'), 'count'),
            array('r.IBLOCK_SECTION_ID', 'brand_id'))
            ->from(array('b_iblock_element', 'a'))
            ->join(array('b_iblock_section_element', 'r'))
            ->on('a.id', '=', 'r.IBLOCK_ELEMENT_ID')
            ->join(array('b_iblock_section', 's'))
            ->on('s.id', '=', 'r.IBLOCK_SECTION_ID')
            ->where('a.iblock_id', '=', $id_block)
            ->and_where('a.active', '=', 'Y')
            ->group_by('s.name')
            ->execute();
    }

    public function getImages($id)
    {
        return DB::select(array('f.file_name', 'image'), 'f.subdir')
            ->from(array('b_iblock_element_property', 'e'))
            ->join(array('b_file', 'f'))
            ->on('f.id', '=', 'e.value')
            ->where('e.iblock_element_id', '=', $id)
            ->execute();
    }

    public function getPropAutoList($id)
    {
        return DB::select('pv.value', 'p.name', 'p.code')
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN',
                array('YEAR', 'PRICE', 'MODIFICATION', 'MILEAGE'))// Можно было взять числовой но так нагляднеем
            ->execute();
    }

    public function getPropAuto($id, $paramList)
    {
        return DB::select('pv.value', 'p.name', 'p.code',
            array('fix.value', 'fixvalue'))
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->join(array('b_iblock_property_enum', 'fix'), 'LEFT')
            ->on('fix.id', '=', 'pv.value_enum')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN', $paramList)
            ->execute();
    }
}