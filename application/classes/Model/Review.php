<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 21.07.2016
 * Time: 12:33
 */

Class Model_Review extends Model_Base {

    protected $_table_name = 'b_iblock_element';

    public function getList()
    {
        return DB::select(
            array('iep.value','name'),
            'ie.date_create',
            'ie.detail_text'
        )->from(array('b_iblock_element_property','iep'))
            ->join(array('b_iblock_element','ie'))
            ->on('ie.id','=','iep.iblock_element_id')
            ->where('iep.iblock_property_id','=',35)
            ->and_where('ie.active','=','Y')
            ->and_where('ie.iblock_id','=',5)
            ->order_by('ie.date_create','DESC')
            ->execute()
            ;
    }

    public function getInfoList()
    {
        return DB::select(
            'ie.id',
            'ie.name',
            'ie.date_create',
            'ie.preview_text'
        )->from(array('b_iblock_element','ie'))
            ->where('ie.active','=','Y')
            ->and_where('ie.iblock_id','=',6)
            ->order_by('ie.date_create','DESC')
            ->execute()
            ;
    }

    public function getGlossList()
    {
        return DB::select(
            'ie.id',
            'ie.name',
            'ie.date_create',
            'ie.preview_text'
        )->from(array('b_iblock_element','ie'))
            ->where('ie.active','=','Y')
            ->and_where('ie.iblock_id','=',7)
            ->order_by('ie.name','ASC')
            ->execute()
            ;
    }



}