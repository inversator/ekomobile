<?php

/**
 * Подбор аккумуляторов по образцу ЭКОАКБ
 */
class Model_Podbor extends Model_Base
{
    protected $auto = array(
        2 => array(
            'car' => 60,
            'truck' => 100,
            'comm' => 101
        )
    );


    public function getList($idAccum)
    {
        return DB::select(
            array('s.name', 'brand_name'),
            array('m.name', 'model_name'),
            array('s.id', 'brand_id'),
            array('m.IBLOCK_SECTION_ID','model_parent'),
            array('o.name','capacity'),
            array('pn.CODE','type')
        )
            ->from(array('b_iblock_element', 'a'))

            ->join(array('b_iblock_element_property', 'p'))
            ->on('p.IBLOCK_ELEMENT_ID', '=', 'a.id')

            ->join(array('b_iblock_element', 'o'))
            ->on('o.ID', '=', 'p.VALUE')

            ->join(array('b_iblock_section', 'm'))
            ->on('m.ID', '=', 'o.IBLOCK_SECTION_ID')

            ->join((array('b_iblock_section', 's')))
            ->on('s.id', '=', 'm.IBLOCK_SECTION_ID')

            ->join(array('b_iblock_property','pn'))
            ->on('pn.id','=','p.IBLOCK_PROPERTY_ID')

            ->where('a.id', '=', $idAccum)

            ->and_where('p.IBLOCK_PROPERTY_ID', 'IN', $this->auto[2])
            ->execute();
    }
}