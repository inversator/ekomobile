<?php

class Model_Brand extends Model_Base
{
    protected $_table_name = 'b_iblock_section';

    public $_fields = array(
        'ID',
        'NAME',
        'IBLOCK_ID',
        'ACTIVE',
        'PICTURE',
        'DESCRIPTION',
        'CODE'
    );

    public function get($code, $block_id = 2) //Москва по умолчанию
    {
        return
            DB::select('b.id', 'b.name', 'b.description', 'b.code', array('f.SUBDIR', 'imgdir'), array('f.file_name', 'image'), array('uf.UF_OPTIONS', 'options'))
                ->from(array($this->_table_name, 'b'))
                ->join(array('b_file', 'f'))
                ->on('f.id', '=', 'b.picture')
                ->join(array('b_uts_iblock_' . $block_id . '_section', 'uf'))
                ->on('uf.VALUE_ID', '=', 'b.id')
                ->where('b.CODE', '=', $code)
                ->and_where('b.IBLOCK_ID', '=', $block_id)
                ->group_by('b.name')
                ->execute();
    }

    public function getMinPrice($brand, $block_id)
    {

        if ($block_id != 2) {
            $sql = "
                SELECT
                    a.NAME as title,
                     p.PRiCE as price,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a

                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y' AND s.code = :CODE
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
        ";
        } else {
            $sql = "
                SELECT
                    a.NAME as title,
                     p.VALUE_NUM as price,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a

                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y' AND s.code = :CODE
                AND p.VALUE_NUM > 0
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
        ";
        }


        $query = DB::query(1, $sql)->param(':CODE', $brand)->param(':block_id', $block_id);
        $units = $query->execute()->as_array();

        $minPrice = 0;

        foreach ($units as $unit) {
            if ($sale = Discount::check($unit)) {
                if (!$minPrice) {
                    $minPrice = $sale;
                } else {
                    if ($sale < $minPrice) $minPrice = $sale;
                }
            } else {
                if (!$minPrice) {
                    $minPrice = $unit['price'];
                } else {
                    if ($unit['price'] < $minPrice) $minPrice = $unit['price'];
                }
            }
        }

        return $minPrice;
    }
}