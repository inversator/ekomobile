<?php

class Model_Action extends Model_Base
{
    protected $_table_name = 'b_iblock_element';
    
    protected $_table_columns = array(
        'ID',
        'ACTIVE',
        'ACTIVE_FROM',
        'NAME',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
    );
}