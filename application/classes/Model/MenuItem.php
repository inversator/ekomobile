<?php

class Model_MenuItem extends Model_Base
{
    protected $_table_name = 'menu_item';
    protected $_table_columns = array(
        'id' => NULL,
        'name' => NULL,
        'alias' => NULL,
        'off' => NULL,
        'type' => NULL,
        'position' => NULL,
    );

    public function getActiveItems($type)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE off <> '1' AND type =:type";

        return DB::query(1, $sql)->param(':type', $type)->execute()->as_array();
    }
    
        public function getAll($type = NULL)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE type =:type";

        return DB::query(1, $sql)->param(':type', $type)->execute();
    }
}