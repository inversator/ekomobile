<?php

class Model_Acces extends Model_Base
{
    protected $_table_name = 'b_iblock_element';
    protected $_table_columns = array(
        'ID',
        'ACTIVE',
        'ACTIVE_FROM',
        'NAME',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
    );

    public $block_id = 9;

    public function get($code, $block_id = 2) //Москва по умолчанию
    {
        return
            DB::select('b.id', 'b.name', 'b.description', 'b.code', array('f.SUBDIR', 'imgdir'), array('f.file_name', 'image'))
                ->from(array('b_iblock_section', 'b'))
                ->join(array('b_file', 'f'))
                ->on('f.id', '=', 'b.picture')
                ->join(array('b_uts_iblock_' . $block_id . '_section', 'uf'))
                ->on('uf.VALUE_ID', '=', 'b.id')
                ->where('b.CODE', '=', $code)
                ->and_where('b.IBLOCK_ID', '=', $block_id)
                ->group_by('b.name')
                ->execute();
    }

    public function getMinPrice($brand, $block_id)
    {
        $sql = "
                SELECT
                    a.NAME as title,
                     p.VALUE_NUM as price,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a

                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y' AND s.code = :CODE
                AND p.VALUE_NUM > 0
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
        ";
    }

    public function getBrandUnits($brand = 'detskie-avtomobilnye-kresla', $block_id = 9)
    {
        $sql = "
                SELECT
                    a.NAME as title,
                    a.id,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a
                LEFT JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                LEFT JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                LEFT JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                LEFT JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y' AND s.code = :CODE
                /*AND p.VALUE_NUM > 0*/
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                ORDER BY a.id DESC
        ";
        $query = DB::query(1, $sql)->param(':CODE', $brand)->param(':block_id', $block_id);
        return $query->execute();

    }

    public function getUnitList($block_id = 9, $brand = 7)
    {
        $sql = 'SELECT a.name, a.id, f.file_name as image, f.subdir '
            . 'FROM b_iblock_element a '
            . 'LEFT JOIN '
            . '(SELECT e.iblock_property_id, e.iblock_element_id, e.value '
            . 'FROM b_iblock_element_property e '
            . 'JOIN b_iblock_property p '
            . 'ON p.id = e.iblock_property_id '
            . 'WHERE p.code = "PHOTOS" '
            . 'ORDER BY e.id) e '
            . 'ON e.iblock_element_id = a.id '
            . 'LEFT JOIN b_file f '
            . 'ON f.id = e.value ';

        if ($brand) {
            $sql .= "JOIN b_iblock_section_element r "
                . "ON a.id = r.iblock_element_id "
                . "AND r.iblock_section_id = :brand ";
        }

        $sql .= 'WHERE a.iblock_id = :block_id '
            . 'AND a.active = "Y" '
            . 'GROUP BY a.id';

        $query = DB::query(1, $sql)->param(':block_id', $block_id);

        if ($brand) {
            $query->param(':brand', $brand);
        }
        return $query->execute();
    }

    public function getUnit($code, $catalog)
    {

        return DB::select(
            'a.name',
            'a.id',
            'a.detail_text',
            'a.preview_text',
            'a.code',
            array('f.file_name', 'image'),
            array('f.subdir', 'image_dir'),
            'p.price',
            array('d.name', 'discount'),
            array('d.VALUE_TYPE', 'disctype'),
            array('d.VALUE', 'discvalue'),
            array('a.IBLOCK_SECTION_ID', 'brand_id')
        )
            ->from(array($this->_table_name, 'a'))
            ->join(array('b_file', 'f'))
            ->on('f.id', '=', 'a.DETAIL_PICTURE')
            ->join(array('b_catalog_price', 'p'))
            ->on('p.PRODUCT_ID', '=', 'a.ID')
            ->join(array('b_catalog_discount2product', 'dr'), 'LEFT')
            ->on('dr.PRODUCT_ID', '=', 'a.ID')
            ->join(array('b_catalog_discount', 'd'), 'LEFT')
            ->on('d.ID', '=', 'dr.DISCOUNT_ID')
            ->where('a.CODE', '=', $code)
            ->and_where('a.ACTIVE', '=', 'Y')
            ->and_where('a.IBLOCK_ID', '=', $catalog)
            ->group_by('a.id')
            ->execute();
    }

    public
    function getBrands($block_id = 2)
    {
        return DB::select('s.name', 's.code', array('f.file_name', 'image'), array('f.subdir', 'image_dir'), array(DB::expr('COUNT(a.id)'), 'count'),
            array('r.IBLOCK_SECTION_ID', 'brand_id'))
            ->from(array('b_iblock_element', 'a'))
            ->join(array('b_iblock_section_element', 'r'))
            ->on('a.id', '=', 'r.IBLOCK_ELEMENT_ID')
            ->join(array('b_iblock_section', 's'))
            ->on('s.id', '=', 'r.IBLOCK_SECTION_ID')
            ->join(array('b_file', 'f'))
            ->on('s.picture', '=', 'f.id')
            ->where('a.iblock_id', '=', $block_id)
            ->and_where('a.active', '=', 'Y')
            ->group_by('s.name')
            ->execute();
    }

    public
    function getImages($id)
    {
        return DB::select(array('f.file_name', 'image'), 'f.subdir')
            ->from(array('b_iblock_element_property', 'e'))
            ->join(array('b_file', 'f'))
            ->on('f.id', '=', 'e.value')
            ->where('e.iblock_element_id', '=', $id)
            ->execute();
    }

    public
    function getPropUnitList($id)
    {
        return DB::select('pv.value', 'p.name', 'p.code')
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN',
                array('YEAR', 'PRICE', 'MODIFICATION', 'MILEAGE'))// Можно было взять числовой но так нагляднеем
            ->execute();
    }

    public function getProps($id, $propList)
    {
        if (!count($propList)) {
            $propList = array();
        }

        return DB::select('pv.value', 'p.name', 'p.code',
            array('fix.value', 'fixvalue'))
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->join(array('b_iblock_property_enum', 'fix'), 'LEFT')
            ->on('fix.id', '=', 'pv.value_enum')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN', $propList)
            ->execute();

    }

    public function getUserFields($section, $block_id)
    {
        return DB::select('*'
        )
            ->from(array('b_uts_iblock_' . $block_id . '_section', 'u'))
            ->where('u.VALUE_ID', '=', $section)
            ->execute();
    }
}