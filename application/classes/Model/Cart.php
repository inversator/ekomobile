<?php


class Model_Cart extends Model_Base
{

    function addToCart($id, $count = 1)
    {

        if(!$count) $count = 1;

        Session::instance();
        if (!empty($_SESSION['cart'][$id])) {

            $_SESSION['cart'][$id]['count'] += $count ? $count : 1;
        } else {
            if (!isset($_SESSION['cart'])) {
                Session::instance()->set('cart',array());
            }

            $_SESSION['cart'][$id] = array();
            $price = Model::factory('Accum')->getPrice($id);

            $_SESSION['cart'][$id]['coast'] = $price;
            $_SESSION['cart'][$id]['count'] = $count ? $count : 1;
        }
    }

    function getCart($block_id = 2)
    {
        if(!$block_id) $block_id = 2;

        Session::instance();
        if (Session::instance()->get('cart')) {
            $units = array_keys(Session::instance()->get('cart'));
        } else {
            $units = array(0);
        }

        return Model::factory('Accum')->getUnitsById($units, $block_id);
    }

    function delFromCart($id, $count = 1)
    {
        Session::instance();
        unset($_SESSION['cart'][$id]);
    }

    function clearCart()
    {
        Session::instance();
        unset($_SESSION['cart']);
    }

    function changeCount($id, $sign)
    {
        Session::instance();
        $_SESSION['cart'][$id]['count'] += $sign;
        if (!$_SESSION['cart'][$id]['count']) unset($_SESSION['cart'][$id]);
    }
}