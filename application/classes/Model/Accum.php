<?php

class Model_Accum extends Model_Base
{
    protected $_table_name = 'b_iblock_element';
    protected $_table_columns = array(
        'ID',
        'ACTIVE',
        'ACTIVE_FROM',
        'NAME',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
    );

    public $block_id = 2;

    public function getBrandUnits($brand = 'ekonom', $block_id = 2)
    {
        if ($block_id != 2) {
            $sql = "    SELECT
                    a.NAME as title,
                    a.id, a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y' AND s.code = :CODE
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                ORDER BY a.id DESC
        ";
        } else {
            $sql = "    SELECT
                    a.NAME as title,
                    a.id, a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.VALUE_NUM as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y' AND s.code = :CODE
                AND p.VALUE_NUM > 0
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                ORDER BY a.id DESC
        ";
        }


        $query = DB::query(1, $sql)->param(':CODE', $brand)->param(':block_id', $block_id);
        return $query->execute();

    }

    public function getUnitsById($ids, $block_id)
    {
        if ($block_id != 2) {

            $sql = "
                SELECT
                    a.NAME as name,
                    a.id, a.code as link,
                    s.code as brand,

                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.id IN :ids
                GROUP BY a.id
                ORDER BY a.id DESC
        ";
        } else {

            $sql = "
                SELECT
                    a.NAME as name,
                    a.id, a.code as link,
                    s.name as brand,

                     p.VALUE_NUM as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.id IN :ids
                AND p.VALUE_NUM > 0

                GROUP BY a.id
                ORDER BY a.id DESC
        ";
        }
        $query = DB::query(1, $sql)->param(':ids', $ids);
        return $query->execute();

    }

    public function getPrice($id)
    {
        $sql = "
                SELECT
                    a.id,
                     p.VALUE_NUM as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a

                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.id = :id
                AND p.VALUE_NUM > 0

                GROUP BY a.id
                ORDER BY a.id DESC
        ";

        $query = DB::query(1, $sql)->param(':id', $id);
        $priceRes = $query->execute();
        if (count($priceRes) > 0) {
            $arr = $priceRes->as_array()[0];
        } else {
            $sql = "
                SELECT
                    a.id,
                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue

                FROM b_iblock_element a

                JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.id = :id

                GROUP BY a.id
                ORDER BY a.id DESC
        ";
            $query = DB::query(1, $sql)->param(':id', $id);
            $priceRes = $query->execute();
            if (count($priceRes) > 0) {
                $arr = $priceRes->as_array()[0];
            } else {
                $arr = array('price' => 0);
            }
        }

        return Discount::check($arr);
    }

    public function getUnit($code, $catalog)
    {
        if ($catalog != 2) {
            $query = DB::select(
                'a.name',
                'a.id',
                'a.detail_text',
                'a.code',
                array('fd.file_name', 'd_image'),
                array('fd.subdir', 'd_image_dir'),

                array('fp.file_name', 'p_image'),
                array('fp.subdir', 'p_image_dir'),

                array('p.PRICE', 'price'),
                array('d.name', 'discount'),
                array('d.VALUE_TYPE', 'disctype'),
                array('d.VALUE', 'discvalue'),
                array('a.IBLOCK_SECTION_ID', 'brand_id'),
                array('s.code', 'brand'),
                array('pr.VALUE', 'analog'),
                array('ac.VALUE', 'analog_capacity')
            )
                ->from(array($this->_table_name, 'a'))
                ->join(array('b_file', 'fd'), 'LEFT')
                ->on('fd.id', '=', 'a.DETAIL_PICTURE')
                ->join(array('b_file', 'fp'), 'LEFT')
                ->on('fp.id', '=', 'a.PREVIEW_PICTURE')
                ->join(array('b_catalog_price', 'p'))
                ->on('p.PRODUCT_ID', '=', 'a.ID')
                ->join(array('b_catalog_discount2product', 'dr'), 'LEFT')
                ->on('dr.PRODUCT_ID', '=', 'a.ID')
                ->join(array('b_catalog_discount', 'd'), 'LEFT')
                ->on('d.ID', '=', 'dr.DISCOUNT_ID')
                ->join(array('b_iblock_section', 's'))
                ->on('a.IBLOCK_SECTION_ID', '=', 's.ID')
                ->join(array('b_iblock_element_property', 'pr'), 'LEFT')
                ->on('a.id', '=', 'pr.IBLOCK_ELEMENT_ID')
                ->on('pr.IBLOCK_PROPERTY_ID', '=', DB::expr(79))
                ->join(array('b_iblock_element_property', 'ac'), 'LEFT')
                ->on('a.id', '=', 'ac.IBLOCK_ELEMENT_ID')
                ->on('ac.IBLOCK_PROPERTY_ID', '=', DB::expr(80))
                ->where('a.CODE', '=', $code)
                ->and_where('a.ACTIVE', '=', 'Y')
                ->and_where('a.IBLOCK_ID', '=', $catalog)
                ->group_by('a.id');
        } else {
            $query = DB::select(
                'a.name',
                'a.id',
                'a.detail_text',
                'a.code',
                array('fd.file_name', 'd_image'),
                array('fd.subdir', 'd_image_dir'),

                array('fp.file_name', 'p_image'),
                array('fp.subdir', 'p_image_dir'),

                array('p.VALUE_NUM', 'price'),
                array('d.name', 'discount'),
                array('d.VALUE_TYPE', 'disctype'),
                array('d.VALUE', 'discvalue'),
                array('a.IBLOCK_SECTION_ID', 'brand_id'),
                array('s.code', 'brand'),
                array('pr.VALUE', 'analog'),
                array('ac.VALUE', 'analog_capacity')
            )
                ->from(array($this->_table_name, 'a'))
                ->join(array('b_file', 'fd'), 'LEFT')
                ->on('fd.id', '=', 'a.DETAIL_PICTURE')
                ->join(array('b_file', 'fp'), 'LEFT')
                ->on('fp.id', '=', 'a.PREVIEW_PICTURE')
                ->join(array('b_iblock_2_index', 'p'))
                ->on('p.ELEMENT_ID', '=', 'a.ID')
                ->join(array('b_catalog_discount2product', 'dr'), 'LEFT')
                ->on('dr.PRODUCT_ID', '=', 'a.ID')
                ->join(array('b_catalog_discount', 'd'), 'LEFT')
                ->on('d.ID', '=', 'dr.DISCOUNT_ID')
                ->join(array('b_iblock_section', 's'))
                ->on('a.IBLOCK_SECTION_ID', '=', 's.ID')
                ->join(array('b_iblock_element_property', 'pr'), 'LEFT')
                ->on('a.id', '=', 'pr.IBLOCK_ELEMENT_ID')
                ->on('pr.IBLOCK_PROPERTY_ID', '=', DB::expr(79))
                ->join(array('b_iblock_element_property', 'ac'), 'LEFT')
                ->on('a.id', '=', 'ac.IBLOCK_ELEMENT_ID')
                ->on('ac.IBLOCK_PROPERTY_ID', '=', DB::expr(80))
                ->where('a.CODE', '=', $code)
                ->and_where('a.ACTIVE', '=', 'Y')
                ->and_where('p.VALUE_NUM', '>', '0')
                ->and_where('a.IBLOCK_ID', '=', $catalog)
                ->group_by('a.id');
        }

        return $query->execute();
    }

    public function getUnitCodeByCapacity($capacity, $polarity, $catalog)
    {
        return DB::select('a.code')
            ->from(array('b_iblock_element', 'a'))
            ->join(array('b_iblock_element_property', 'cap_el'))
            ->on('a.ID', '=', 'cap_el.IBLOCK_ELEMENT_ID')
            ->join(array('b_iblock_section', 's'))
            ->on('a.IBLOCK_SECTION_ID', '=', 's.ID')
            ->join(array('b_iblock_property', 'name_cap'))
            ->on('cap_el.IBLOCK_PROPERTY_ID', '=', 'name_cap.id')
            ->join(array('b_iblock_element_property', 'pol_el'))
            ->on('a.ID', '=', 'pol_el.IBLOCK_ELEMENT_ID')
            ->join(array('b_iblock_property', 'name_pol'))
            ->on('pol_el.IBLOCK_PROPERTY_ID', '=', 'name_pol.id')
            ->where_open()
            ->where('name_cap.CODE', '=', 'CAPACITY')
            ->or_where('name_pol.CODE', '=', 'POLARITY')
            ->where_close()
            ->where_open()
            ->where('cap_el.VALUE', 'BETWEEN', array($capacity - 2, $capacity + 2))
            ->and_where('pol_el.VALUE', '=', $polarity)
            ->where_close()
            ->and_where('a.IBLOCK_ID', '=', $catalog)
            ->and_where('s.code', '=', 'econom')
            ->group_by('a.id')
            ->execute();
    }

    public function getUserFields($section, $block_id)
    {
        if ($block_id != 2) {
            $query = DB::select(
                'u.UF_OPTIONS',
                'u.UF_SP', 'u.UF_WARRANTY',
                'u.UF_H1'
            )
                ->from(array('b_uts_iblock_' . $block_id . '_section', 'u'))
                ->where('u.VALUE_ID', '=', $section);
        } else {
            $query = DB::select(
                'u.UF_BROWSER_TITLE',
                'u.UF_KEYWORDS',
                'u.UF_META_DESCRIPTION',
                'u.UF_OPTIONS',
                'u.UF_SP', 'u.UF_WARRANTY',
                'u.UF_H1'
            )
                ->from(array('b_uts_iblock_' . $block_id . '_section', 'u'))
                ->where('u.VALUE_ID', '=', $section);
        }

        return $query->execute();
    }

    public function getBrands($block_id = 2)
    {
        return DB::select('s.name', 's.code', array('f.file_name', 'image'), array('f.subdir', 'image_dir'), array(DB::expr('COUNT(a.id)'), 'count'),
            array('r.IBLOCK_SECTION_ID', 'brand_id'))
            ->from(array('b_iblock_element', 'a'))
            ->join(array('b_iblock_section_element', 'r'))
            ->on('a.id', '=', 'r.IBLOCK_ELEMENT_ID')
            ->join(array('b_iblock_section', 's'))
            ->on('s.id', '=', 'r.IBLOCK_SECTION_ID')
            ->join(array('b_file', 'f'))
            ->on('s.picture', '=', 'f.id')
            ->where('a.iblock_id', '=', $block_id)
            ->and_where('a.active', '=', 'Y')
            ->and_where('s.active', '=', 'Y')
            ->group_by('s.name')
            ->order_by('s.id')
            ->execute();
    }

    public function getProps($id, $propList)
    {
        if (!count($propList)) {
            $propList = array();
        }

        return DB::select('pv.value', 'p.name', 'p.code',
            array('fix.value', 'fixvalue'))
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->join(array('b_iblock_property_enum', 'fix'), 'LEFT')
            ->on('fix.id', '=', 'pv.value_enum')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN', $propList)
            ->execute();
    }

    public function getActionUnits($block_id = 2)
    {
        if ($block_id == 2) {
            $sql = "
                SELECT
                    a.id,
                    a.NAME as title,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.VALUE_NUM as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue,
                     s.code as brand

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                RIGHT JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                RIGHT JOIN b_iblock_property bip
                ON bip.id = rp.iblock_property_id

                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y'
                AND p.VALUE_NUM > 0
                AND s.IBLOCK_ID = :block_id
                AND bip.code = 'LABEL_ACTION'
                GROUP BY a.id
                ORDER BY a.id DESC
                LIMIT 3
        ";
        } else {
            $sql = "
                SELECT
                    a.id,
                    a.NAME as title,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue,
                     s.code as brand

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID
                RIGHT JOIN b_iblock_element_property rp
                ON rp.IBLOCK_ELEMENT_ID = a.ID
                RIGHT JOIN b_iblock_property bip
                ON bip.id = rp.iblock_property_id

                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y'
                AND s.IBLOCK_ID = :block_id
                AND bip.code = 'LABEL_ACTION'
                GROUP BY a.id
                ORDER BY a.id DESC
                LIMIT 3
        ";
        }

        $query = DB::query(1, $sql)->param(':block_id', $block_id);
        return $query->execute();

    }

    public function getNewUnits($block_id = 2)
    {
        if($block_id != 2) {
            $sql = "
                SELECT
                    a.id,
                    a.NAME as title,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue,
                     s.code as brand

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID

                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y'

                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                ORDER BY a.date_create DESC
                LIMIT 3
        ";
        } else {
            $sql = "
                SELECT
                    a.id,
                    a.NAME as title,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.VALUE_NUM as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue,
                     s.code as brand

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID


                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                WHERE
                a.ACTIVE = 'Y'
                AND p.VALUE_NUM > 0
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                ORDER BY a.date_create DESC
                LIMIT 3
        ";
        }


        $query = DB::query(1, $sql)->param(':block_id', $block_id);
        return $query->execute();

    }

    public function getHitUnits($block_id = 2)
    {

        if ($block_id != 2) {
            $sql = "
                SELECT
                    a.id,
                    a.NAME as title,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.PRICE as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue,
                     s.code as brand

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID

                JOIN b_catalog_price p
                ON a.ID = p.PRODUCT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                JOIN b_iblock_element_property iep
                ON a.id = iep.iblock_element_id

                JOIN b_iblock_property ip
                ON ip.id = iep.iblock_property_id
                AND ip.code = 'ST'
                AND ip.iblock_id = :block_id
                WHERE
                a.ACTIVE = 'Y'

                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                LIMIT 3
        ";
        }
        else {
            $sql = "
                SELECT
                    a.id,
                    a.NAME as title,
                    a.code as link,
                     f.FILE_NAME as image,
                     f.SUBDIR as imgdir,
                     p.VALUE_NUM as price,
                     d.name as discount,
                     d.VALUE_TYPE as disctype,
                     d.VALUE as discvalue,
                     s.code as brand

                FROM b_iblock_element a
                JOIN b_file f
                ON f.id = a.PREVIEW_PICTURE
                JOIN b_iblock_section s
                ON s.id = a.IBLOCK_SECTION_ID


                JOIN b_iblock_2_index p
                ON a.ID = p.ELEMENT_ID
                LEFT JOIN b_catalog_discount2product as dr
                ON dr.PRODUCT_ID = a.id
                LEFT JOIN b_catalog_discount d
                ON d.id = dr.DISCOUNT_ID

                JOIN b_iblock_element_property iep
                ON a.id = iep.iblock_element_id

                JOIN b_iblock_property ip
                ON ip.id = iep.iblock_property_id
                AND ip.code = 'ST'
                AND ip.iblock_id = :block_id
                WHERE
                a.ACTIVE = 'Y'
                AND p.VALUE_NUM > 0
                AND s.IBLOCK_ID = :block_id
                GROUP BY a.id
                LIMIT 3
        ";
        }


        $query = DB::query(1, $sql)->param(':block_id', $block_id);
        return $query->execute();

    }
}