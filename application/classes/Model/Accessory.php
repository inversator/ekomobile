<?php

class Model_Accessory extends Model_Base
{
    protected $_table_name = 'b_iblock_element';
    protected $_table_columns = array(
        'ID',
        'ACTIVE',
        'ACTIVE_FROM',
        'NAME',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
    );

    public $block_id = 9;

    public function getAutoList($block_id = 2, $brand = NULL)
    {
        $sql = 'SELECT a.name, a.id, f.file_name as image, f.subdir '
            . 'FROM b_iblock_element a '
            . 'LEFT JOIN '
            . '(SELECT e.iblock_property_id, e.iblock_element_id, e.value '
            . 'FROM b_iblock_element_property e '
            . 'JOIN b_iblock_property p '
            . 'ON p.id = e.iblock_property_id '
            . 'WHERE p.code = "PHOTOS" '
            . 'ORDER BY e.id) e '
            . 'ON e.iblock_element_id = a.id '
            . 'LEFT JOIN b_file f '
            . 'ON f.id = e.value ';

        if ($brand) {
            $sql .= "JOIN b_iblock_section_element r "
                . "ON a.id = r.iblock_element_id "
                . "AND r.iblock_section_id = :brand ";
        }

        $sql .= 'WHERE a.iblock_id = :block_id '
            . 'AND a.active = "Y" '
            . 'GROUP BY a.id';

        $query = DB::query(1, $sql)->param(':block_id', $block_id);

        if ($brand) {
            $query->param(':brand', $brand);
        }
        return $query->execute();
    }

    public function getAuto($id)
    {
        return DB::select('a.name', 'a.id', 'a.detail_text')
            ->from(array($this->_table_name, 'a'))
            ->where('a.ID', '=', $id)
            ->group_by('a.id')
            ->execute();
    }

    public function getCats($block_id = 9)
    {
        return DB::select('s.name', 's.code', array('f.file_name', 'image'), array('f.subdir', 'image_dir'), array(DB::expr('COUNT(a.id)'), 'count'),
            array('r.IBLOCK_SECTION_ID', 'cat_id'))
            ->from(array('b_iblock_element', 'a'))
            ->join(array('b_iblock_section_element', 'r'))
            ->on('a.id', '=', 'r.IBLOCK_ELEMENT_ID')
            ->join(array('b_iblock_section', 's'))
            ->on('s.id', '=', 'r.IBLOCK_SECTION_ID')
            ->join(array('b_file', 'f'))
            ->on('s.picture', '=', 'f.id')
            ->where('a.iblock_id', '=', $block_id)
            ->and_where('a.active', '=', 'Y')
            ->and_where('s.active','=','Y')
            ->group_by('s.name')
            ->execute();
    }

    public function getImages($id)
    {
        return DB::select(array('f.file_name', 'image'), 'f.subdir')
            ->from(array('b_iblock_element_property', 'e'))
            ->join(array('b_file', 'f'))
            ->on('f.id', '=', 'e.value')
            ->where('e.iblock_element_id', '=', $id)
            ->execute();
    }

    public function getPropAutoList($id)
    {
        return DB::select('pv.value', 'p.name', 'p.code')
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN',
                array('YEAR', 'PRICE', 'MODIFICATION', 'MILEAGE'))// Можно было взять числовой но так нагляднеем
            ->execute();
    }

    public function getPropAuto($id, $paramList)
    {
        return DB::select('pv.value', 'p.name', 'p.code',
            array('fix.value', 'fixvalue'))
            ->from(array('b_iblock_element_property', 'pv'))
            ->join(array('b_iblock_property', 'p'))
            ->on('pv.iblock_property_id', '=', 'p.id')
            ->join(array('b_iblock_property_enum', 'fix'), 'LEFT')
            ->on('fix.id', '=', 'pv.value_enum')
            ->where('pv.iblock_element_id', '=', $id)
            ->and_where('p.code', 'IN', $paramList)
            ->execute();
    }
}