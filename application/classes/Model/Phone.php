<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Phone extends Model_Base
{
    protected function rules()
    {
        return array(
            'phone' => array(
                array('not_empty'),
                array('phone', array(':value', TRUE))
            )
        );
    }
}