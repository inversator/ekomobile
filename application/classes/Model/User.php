<?php

class Model_User extends Model_Base
{
    public $fields = array (
        'ID',
        'LOGIN',
        'PASSWORD'
    );

    protected $_table_name = 'b_user';

    public function get($login)
    {
        return DB::select()->from($this->_table_name)
            ->where('login','=',$login)
            ->as_object()
            ->execute();
    }

    public function add($user)
    {
        $salt = Room::randString(8, array(
            "abcdefghijklnmopqrstuvwxyz",
            "ABCDEFGHIJKLNMOPQRSTUVWXYZ",
            "0123456789",
            ",.<>/?;:[]{}\\|~!@#\$%^&*()-_+=",
        ));

        $fields = array(
            'login' => $user['login'],
            'password' => $salt.md5($salt.$user['password']),
            'checkword' => md5(md5(uniqid(rand(), true).uniqid())),
            'name' => $user['name'],
            'last_name' => $user['last_name']
        );

        return DB::insert($this->_table_name, array('login','password','checkword','name','last_name',))
            ->values($fields)
            ->execute();
    }

    public function add_group($id, $group)
    {
        return DB::insert('b_user_group', array('USER_ID','GROUP_ID'))
            ->values(array($id,$group))
            ->execute();
    }

    protected function rules()
    {
        return array(

        );
    }

}