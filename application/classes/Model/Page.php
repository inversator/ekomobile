<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Page extends Model_Base
{
    protected $_table_name = 'pages'; //Если имя иное
    //protected $_primary_key= 'id';
    protected $_table_columns = array(
        'id' => NULL,
        'meta_k' => NULL,
        'meta_d' => NULL,
        'bTitle' => NULL,
        'pTitle' => NULL,
        'desc' => NULL,
        'text' => NULL,
        'off' => NULL,
        'url' => NULL,
    );

    protected function rules()
    {
        return array(
            'pTitle' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'bTitle' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'meta_k' => array(
                array('max_length', array(':value', 255)),
            ),
            'meta_d' => array(
                array('max_length', array(':value', 255)),
            ),
            'url' => array(
                array('not_empty'),
                array('english_num'),
                array('max_length', array(':value', 25)),
                array('unic_field', array(':value', ':field', 'page', parent::$specimen['id']))
            ),
        );
    }
}