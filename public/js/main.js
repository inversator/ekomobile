function AjaxFormRequest(result_id, form_id, url) {
    jQuery.ajax({
        url: url, //Адрес подгружаемой страницы
        type: "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: jQuery("#" + form_id).serialize(),
        success: function (response) { //Если все нормально
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>" + response + "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        },
        error: function (response) { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function AjaxResForm(result_id, form_id, url) {
    jQuery.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: jQuery("#" + form_id).serialize(),
        success: function (response) {

            if (response[0] !== 0) {
                document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Сообщение отправлено<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
                $('#formCargo').remove();
            } else {
                var message = response[1];

                if (message !== undefined) {
                    var goodFields = arrDiff(message, jQuery("#" + form_id).serialize());

                    for (var field in goodFields) {
                        $('div[own="' + field + '"]').attr('class', 'form-group');
                        $('div[own="' + field + '"] error').html('');
                    }

                    for (var key in message) {
                        $('div[own="' + key + '"]').attr('class', 'form-group has-error');
                        $('div[own="' + key + '"] error').html(message[key]);
                    }
                }
            }
        },
        error: function () { //Если ошибка
            document.getElementById(result_id).innerHTML = "<div class='alert alert-warning fade in' id='result_div_id'>Ошибка при отправке формы<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button></div>";
        }
    });
}

function changeAutoSelect(type, lavel) {

    $('fieldset').prop('disabled', true);
    $('.loadIndicator').css('display', 'block');

    switch (lavel) {
        case 'brand':
            var data = {
                type: type,
                brand: $('#' + type + ' #brand option:selected').val()
            };
            break;
        case 'model':
            var data = {
                type: type,
                brand: $('#' + type + ' #brand option:selected').val(),
                model: $('#' + type + ' #model option:selected').val()
            };
            break;
        case 'modify':
            var data = {
                type: type,
                brand: $('#' + type + ' #brand option:selected').val(),
                model: $('#' + type + ' #model option:selected').val(),
                modify: $('#' + type + ' #modify option:selected').val()
            };
            break;
    }

    if (data.modify) {
        $.ajax({
            url: "/auto/getAccums",
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (accums) {
                if (accums !== undefined && accums[0] !== undefined) {
                    var html = ""
                    for (var x in accums) {


                        if (undefined !== accums[x].discvalue && accums[x].discvalue > 0) {
                            switch (accums[x].disctype) {
                                case 'P':
                                    var newPrice = accums[x].price - accums[x].price / 100 * accums[x].discvalue;
                                    break;

                                case 'F':
                                    var newPrice = accums[x].price - accums[x].discvalue;

                                case 'S':
                                    var newPrice;
                            }
                            var price = "<s class='text-danger'>" + accums[x].price + "</s>" + newPrice;
                        } else {
                            var price = accums[x].price;
                        }

                        html += '' +
                            '<div class="col-xs-12 col-sm-4 faceUnits">' +

                            '<div class="image">' +
                            '<a href="/accum/' + accums[x].brand + '/' + accums[x].code + '/">' +
                            ' <img alt="" src="http://www.ekoakb.ru/upload/' + accums[x].image_dir + '/' + accums[x].image + '">' +
                            '</a></div>' +
                            '<div class="title"><a href="/accum/' + accums[x].brand + '/' + accums[x].code + '/">' + accums[x].name + '</a></div>' +

                            '<div class="priceBlock">' +
                            '<div class="price col-xs-8">' + price + ' руб.</div>' +
                            '<div class="buy col-xs-4 btn btn-success" onclick="addToCart(' + accums[x].id + ')">В корзину</div>' +
                            '</div>';

                        if (accums[x].properties !== undefined) {
                            html += '<table class="table properties">';
                            for (var y in accums[x].properties) {
                                html += '<tr><td>' + accums[x].properties[y].name + '</td><td>' + accums[x].properties[y].value + '</td></tr>'
                            }

                            html += '</table>';
                        }
                        html += '</div>';

                    }

                } else {
                    $('#units').html('<p class="label label-danger"> — Нет подходящих аккумуляторов — </p>');
                }

                $('#units').html(html);
                $('fieldset').prop('disabled', false);
                $('.loadIndicator').css('display', 'none');
            }

        });
    } else if (data.model) {
        $.ajax({
            url: "/auto/getModify",
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (models) {
                if (models.modifs !== undefined) {

                    var options = '<option value="">Модификация</option>';

                    for (var i in models.modifs) {
                        options += '<option value=' + models.modifs[i].id + '>' + models.modifs[i].name + '</option>';
                    }

                    $('#' + type + ' #modify').html(options);
                    $('fieldset').prop('disabled', false);
                    $('.loadIndicator').css('display', 'none');

                    var accums = models.accums;

                    if (accums !== undefined && accums[0] !== undefined) {
                        var html = ""

                        for (var x in accums) {
                            if (accums[x].discvalue !== undefined && accums[x].discvalue > 0) {
                                switch (accums[x].disctype) {
                                    case 'P':
                                        var newPrice = accums[x].price - accums[x].price / 100 * accums[x].discvalue;
                                        break;

                                    case 'F':
                                        var newPrice = accums[x].price - accums[x].discvalue;

                                    case 'S':
                                        var newPrice;
                                }
                                var price = "<s class='text-danger'>" + accums[x].price + "</s>" + newPrice;
                            } else {
                                var price = accums[x].price;
                            }

                            html += '' +
                                '<div class="col-xs-12 col-sm-4 faceUnits">' +

                                '<div class="image">' +
                                '<a href="/accum/' + accums[x].brand + '/' + accums[x].code + '/">' +
                                '<img alt="" src="http://www.ekoakb.ru/upload/' + accums[x].image_dir + '/' + accums[x].image + '">' +
                                '</a></div>' +
                                '<div class="title"><a href="/accum/' + accums[x].brand + '/' + accums[x].code + '/">' + accums[x].name + '</a></div>' +

                                '<div class="priceBlock">' +
                                '<div class="price col-xs-8">' + price + ' руб.</div>' +
                                '<div class="buy col-xs-4 btn btn-success" onclick="addToCart(' + accums[x].id + ')">В корзину</div>' +
                                '</div>';

                            if (accums[x].properties !== undefined) {
                                html += '<table class="table properties">';
                                for (var y in accums[x].properties) {
                                    html += '<tr><td>' + accums[x].properties[y].name + '</td><td>' + accums[x].properties[y].value + '</td></tr>'
                                }

                                html += '</table>';
                            }
                            html += '</div>';
                            $('#units').html(html);
                        }

                    } else {
                        $('#units').html('<p class="label label-danger"> — Нет подходящих аккумуляторов — </p>');
                    }

                }
            }

        });
    } else {
        $('#units').html("");

        $.ajax({
            url: "/auto/getModels",
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (models) {
                if (models !== undefined) {

                    var options = '<option value="">Модель авто</option>';
                    for (var i in models) {
                        options += '<option value=' + models[i].id + '>' + models[i].name + '</option>';
                    }

                    $('#' + type + ' #model').html(options);
                    $('fieldset').prop('disabled', false);
                    $('.loadIndicator').css('display', 'none');

                }
            }

        });
    }


}
function makeOrder() {
    if ($('input[name="call"]').prop("checked"))
        call = 1;
    else
        call = 0;
    var data = {
        id: '',
        name: $('input[name="name"]').val(),
        phone: $('input[name="phone"]').val(),
        email: $('input[name="email"]').val(),
        index: $('input[name="index"]').val(),
        city: $('input[name="city"]').val(),
        address: $('textarea[name="address"]').val(),
        comment: $('textarea[name="comment"]').val(),
        call: call,
        delivery: $('input[name="delivery"]:checked').val()
    };

    $.ajax({
        url: "/order/make",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message['result']) {
                var html = '';

                html += '<div class="alert alert-success">';
                html += '<strong>Заказ оформлен!</strong> Ждите сообщения или звонка от наших менеджеров.';
                html += '</div>';

                $('#accordion').html(html);
                $.get("/cart/clearCart", function () {
                    updateCart();
                });

            } else {
                if (message['error'] !== undefined) {
                    for (var key in message['error']) {
                        $('div[id="' + key + '"]').attr('class', 'form-group has-error');
                    }
                }
            }
        }
    });

}
function oneClick() {

    var data = {
        accum: $('#accumName').text(),
        cost: $('#accumCost').text(),
        name: $('input[name="name"]').val(),
        phone: $('input[name="phone"]').val(),
        email: $('input[name="email"]').val(),
        index: $('input[name="index"]').val(),
        city: $('#city option:selected').val(),
        comment: $('textarea[name="comment"]').val()
    };

    $.ajax({
        url: "/sender/oneClick",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message['result']) {
                var html = '';

                html += '<div class="alert alert-success">';
                html += '<strong>Заказ оформлен!</strong> Ждите сообщения или звонка от наших менеджеров.';
                html += '</div>';

                $('#oneClick .modal-body').html(html);
                $('#oneClick .modal-footer .btn-primary').remove();
            } else {
                if (message['error'] !== undefined) {
                    for (var key in message['error']) {
                        $('div[id="' + key + '"]').attr('class', 'form-group has-error');
                    }
                }
            }
        }
    });

}


function contactMe() {

    var data = {
        name: $('input[name="name"]').val(),
        phone: $('input[name="phone"]').val(),
        email: $('input[name="email"]').val(),
        comment: $('textarea[name="comment"]').val()
    };

    $.ajax({
        url: "/sender/contactMe",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message['result']) {
                $('#contactForm').html('<p class="label label-success">Ваше сообщение отправлено. Ожидайте ответа от наших менеджеров</p>');
            } else {
                if (message['error'] !== undefined) {
                    for (var key in message['error']) {
                        $('div[id="' + key + '"]').attr('class', 'form-group has-error');
                    }
                }
            }
        }
    });

}

function custom() {

    var data = {
        accum: $('#accumCustomName').text(),
        cost: $('#accumCustomCost').text(),
        customName: $('input[name="customName"]').val(),
        customPhone: $('input[name="customPhone"]').val(),
        customEmail: $('input[name="customEmail"]').val(),
        customIndex: $('input[name="customIndex"]').val(),
        customCity: $('#customCity option:selected').val(),
        customComment: $('textarea[name="customComment"]').val()
    };

    $.ajax({
        url: "/sender/custom",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message['result']) {
                var html = '';

                html += '<div class="alert alert-success">';
                html += '<strong>Заказ оформлен!</strong> Ждите сообщения или звонка от наших менеджеров.';
                html += '</div>';

                $('#custom .modal-body').html(html);
                $('#custom .modal-footer .btn-primary').remove();
            } else {
                if (message['error'] !== undefined) {
                    for (var key in message['error']) {
                        $('div[id="' + key + '"]').attr('class', 'form-group has-error');
                    }
                }
            }
        }
    });

}

function partnerMe() {

    var data = {
        name: $('input[name="name"]').val(),
        phone: $('input[name="phone"]').val(),
        email: $('input[name="email"]').val(),
        comment: $('input[name="comment"]').val()
    };

    $.ajax({
        url: "/sender/partner",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message['result']) {
                var html = '';

                html += '<div class="alert alert-success">';
                html += '<strong>Заявка отправлена!</strong> Ждите сообщения или звонка от наших менеджеров.';
                html += '</div>';

                $('#partnerModal .modal-body').html(html);
                $('#partnerModal .modal-footer .btn-primary').remove();
            } else {
                if (message['error'] !== undefined) {
                    for (var key in message['error']) {
                        $('div[id="' + key + '"]').attr('class', 'form-group has-error');
                    }
                }
            }
        }
    });

}

function callMe() {
    var data = {
        phone: $('#callMe').val()
    };

    $.ajax({
        url: "/sender/callme",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message !== undefined) {

                if (message['result']) {
                    html = '<h4>Спасибо за обращение. Наши менеджеры свяжутся с Вами в ближайшее время</h4>';
                    $('#callMeBlock').html(html);

                }
                else {
                    $('div[own="phone"]').attr('class', 'form-group input-group has-error');
                    //$('div[own="phone"] error').html(message[key]);
                }
            }
        }
    });
}

function callMe2() {
    var data = {
        name: $('#callMeName').val(),
        phone: $('#callMePhone').val()
    };

    $.ajax({
        url: "/sender/callme",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {
            if (message !== undefined) {

                if (message['result']) {
                    html = '<h4 class="text-success">Спасибо за обращение. Наши менеджеры свяжутся с Вами в ближайшее время</h4>';
                    $('#callFree .modal-body').html(html);
                    $('#callFree .btn-primary').remove();
                }
                else {
                    $('div[own="phoneCall"]').attr('class', 'form-group has-error');
                    //$('div[own="phone"] error').html(message[key]);
                }
            }
        }
    });
}

function checkField() {

    var data = {
        name: $('input[name="name"]').val(),
        phone: $('input[name="phone"]').val(),
        email: $('input[name="email"]').val(),
        comment: $('textarea[name="comment"]').val()
    };

    $.ajax({
        url: "/sender/check",
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function (message) {

            if (message !== undefined) {
                var goodFields = arrDiff(message, data);

                for (var field in goodFields) {
                    $('div[own="' + field + '"]').attr('class', 'form-group');
                    $('div[own="' + field + '"] error').html('');
                }

                for (var key in message) {
                    $('div[own="' + key + '"]').attr('class', 'form-group has-error');
                    $('div[own="' + key + '"] error').html(message[key]);
                }
            }
        }
    });
}

// Это разность массивов
function arrDiff(arr1, arr2) {
    for (var key in arr1) {
        if (arr2[key] !== undefined) {
            delete arr2[key];
        }
    }
    return arr2;
}

// Это дополнение jq для GET параметров
$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});

function addToCart(id) {
    //$('#load').show();
    $.ajax({
        type: 'POST',
        url: '/cart/addToCart',
        async: false,
        data: {
            id: id
        }
    }).done(function () {
        //alert('ЗАПРОС ПРОШЕЛ, ждем куки');
        updateCart()
    }).error(
        function (err) {
            //alert(err);
        });
}

function addCartSuccess(data) {
    updateCart();
}
function updateCart() {
    $.ajax({
        url: "/cart/updateCart",
        dataType: "json",
        async: false
    }).done(function (html) {
        $('#cart').html(html['cart']);
        $('.fullCartBlock').html(html['block']);
        //$('#load').hide();
    }).error(function (xhr, status, error) {
        alert(xhr.responseText + '|\n' + status + '|\n' + error);
        //$('#load').hide();
    });
}
function delFromCart(id) {
    $.ajax({
        url: "/cart/delFromCart",
        async: false,
        data: {id: id},
        success: function () {
            updateCart();
        }
    });
}

function changeCountCart(id, sign) {
    $.ajax({
        url: "/cart/changeCount",
        async: false,
        data: {id: id, sign: sign},
        success: function () {
            updateCart();
        }
    });
}
function dump(obj) {
    var out = "";
    if (obj && typeof(obj) == "object") {
        for (var i in obj) {
            out += i + ": " + obj[i] + "n";
        }
    } else {
        out = obj;
    }
    alert(out);
}

function print_r(array, return_val) {	// Prints human-readable information about a variable
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // + namespaced by: Michael White (http://crestidg.com)

    var output = "", pad_char = " ", pad_val = 4;

    var formatArray = function (obj, cur_depth, pad_val, pad_char) {
        if (cur_depth > 0)
            cur_depth++;

        var base_pad = repeat_char(pad_val * cur_depth, pad_char);
        var thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
        var str = "";

        if (obj instanceof Array) {
            str += "Array\n" + base_pad + "(\n";
            for (var key in obj) {
                if (obj[key] instanceof Array) {
                    str += thick_pad + "[" + key + "] => " + formatArray(obj[key], cur_depth + 1, pad_val, pad_char);
                } else {
                    str += thick_pad + "[" + key + "] => " + obj[key] + "\n";
                }
            }
            str += base_pad + ")\n";
        } else {
            str = obj.toString(); // They didn't pass in an array.... why? -- Do the best we can to output this object.
        }
        ;

        return str;
    };

    var repeat_char = function (len, char) {
        var str = "";
        for (var i = 0; i < len; i++) {
            str += char;
        }
        ;
        return str;
    };

    output = formatArray(array, 0, pad_val, pad_char);

    if (return_val !== true) {
        document.write("<pre>" + output + "</pre>");
        return true;
    } else {
        return output;
    }
}
